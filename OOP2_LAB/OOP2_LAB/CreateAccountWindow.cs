﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace OOP2_LAB
{
    public partial class CreateAccountWindow : Form
    {
        private string userFilePath;
        public CreateAccountWindow()
        {
            InitializeComponent();
            userFilePath = LoginWindow.userFilePath;
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            string username = tbCreateUsername.Text;
            string password = Util.ComputeSha256Hash(tbCreatePassword.Text);
            int userType;
            if (LoginWindow.userList.Count == 0)
            {
                userType = 0;
            }
            else
            {
                userType = 2;
            }
            PersonalInformation newPI = new PersonalInformation();
            User newUser = new User(username, password, false, userType,newPI);
            LoginWindow.userList.Add(newUser);
            Util.SaveCsv(LoginWindow.userList, LoginWindow.userFilePath);
            //SaveNewUser(username, password, userType);
            string msg = "You have created a new account!" + Environment.NewLine + "Please click button to continue.";
            MessageBox.Show(msg);
            this.Hide();
            LoginWindow loginWindow = new LoginWindow();
            loginWindow.Show();
        }

        private void tbCreateUsername_TextChanged(object sender, EventArgs e)
        {
            foreach (User user in LoginWindow.userList)
            {
                if (tbCreateUsername.Text == user.Username)
                {
                    lbCheckUsername.Text = "You can't use this username.";
                    lbCheckUsername.ForeColor = Color.Red;
                    btnCreate.Enabled = false;
                    return;
                }
            }
            if (tbCreateUsername.Text == "")
            {
                lbCheckUsername.Text = "";
            }
            else
            {
                lbCheckUsername.Text = "You can use this username.";
                lbCheckUsername.ForeColor = Color.Green;
            }
        }

        private void CreateAccountWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
    }
}
