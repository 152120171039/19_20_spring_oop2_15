﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OOP2_LAB
{
    public partial class MainWindow : Form
    {

        public MainWindow()
        {
            InitializeComponent();
            if (LoginedUser.getInstance().User.UserType == 0 || LoginedUser.getInstance().User.UserType == 1)
            {
                btnUserManagement.Visible = true;
            }
        }

        private void btnLogout_Click(object sender, EventArgs e)
        {
            foreach (User user in LoginWindow.userList)
            {
                user.RememberMe = false;
            }
            string userFilePath = LoginWindow.userFilePath;
            Util.SaveCsv(LoginWindow.userList, userFilePath);
            this.Hide();
            LoginWindow loginWindow = new LoginWindow();
            loginWindow.Show();
        }

        private void MainWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void btnUserManagement_Click(object sender, EventArgs e)
        {
            this.Hide();
            UMWindow uMWindow = new UMWindow();
            uMWindow.Show();
        }

        private void btn_Phone_book_Click(object sender, EventArgs e)
        {
            this.Hide();
            PhoneBookWindow phoneBookWindow = new PhoneBookWindow();
            phoneBookWindow.Show();
        }

        private void btn_Notes_Click(object sender, EventArgs e)
        {
            this.Hide();
            NotesWindow notesWindow = new NotesWindow();
            notesWindow.Show();
        }

        private void btnPI_Click(object sender, EventArgs e)
        {
            this.Hide();
            PIWindow pIWindow = new PIWindow();
            pIWindow.Show();
        }

        private void btn_salary_Click(object sender, EventArgs e)
        {
            this.Hide();
            SalaryCalculator Sal_cal = new SalaryCalculator();
            Sal_cal.Show();
        }

        private void btn_reminders_Click(object sender, EventArgs e)
        {
            this.Hide();
            RemindersWindow reminders = new RemindersWindow();
            reminders.Show();
        }
    }
}
