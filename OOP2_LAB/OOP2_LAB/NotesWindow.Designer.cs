﻿namespace OOP2_LAB
{
    partial class NotesWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_list = new System.Windows.Forms.Button();
            this.btn_create = new System.Windows.Forms.Button();
            this.btn_update = new System.Windows.Forms.Button();
            this.btn_delete = new System.Windows.Forms.Button();
            this.listView_Notes = new System.Windows.Forms.ListView();
            this.tb_note = new System.Windows.Forms.TextBox();
            this.btn_mainwindow = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn_list
            // 
            this.btn_list.Location = new System.Drawing.Point(169, 27);
            this.btn_list.Name = "btn_list";
            this.btn_list.Size = new System.Drawing.Size(105, 53);
            this.btn_list.TabIndex = 0;
            this.btn_list.Text = "List";
            this.btn_list.UseVisualStyleBackColor = true;
            this.btn_list.Click += new System.EventHandler(this.btn_list_Click);
            // 
            // btn_create
            // 
            this.btn_create.Location = new System.Drawing.Point(280, 27);
            this.btn_create.Name = "btn_create";
            this.btn_create.Size = new System.Drawing.Size(105, 53);
            this.btn_create.TabIndex = 1;
            this.btn_create.Text = "Create";
            this.btn_create.UseVisualStyleBackColor = true;
            this.btn_create.Click += new System.EventHandler(this.btn_create_Click);
            // 
            // btn_update
            // 
            this.btn_update.Location = new System.Drawing.Point(391, 27);
            this.btn_update.Name = "btn_update";
            this.btn_update.Size = new System.Drawing.Size(105, 53);
            this.btn_update.TabIndex = 2;
            this.btn_update.Text = "Update";
            this.btn_update.UseVisualStyleBackColor = true;
            this.btn_update.Click += new System.EventHandler(this.btn_update_Click);
            // 
            // btn_delete
            // 
            this.btn_delete.Location = new System.Drawing.Point(502, 27);
            this.btn_delete.Name = "btn_delete";
            this.btn_delete.Size = new System.Drawing.Size(105, 53);
            this.btn_delete.TabIndex = 3;
            this.btn_delete.Text = "Delete";
            this.btn_delete.UseVisualStyleBackColor = true;
            this.btn_delete.Click += new System.EventHandler(this.btn_delete_Click);
            // 
            // listView_Notes
            // 
            this.listView_Notes.FullRowSelect = true;
            this.listView_Notes.GridLines = true;
            this.listView_Notes.HideSelection = false;
            this.listView_Notes.Location = new System.Drawing.Point(48, 241);
            this.listView_Notes.Name = "listView_Notes";
            this.listView_Notes.Size = new System.Drawing.Size(709, 167);
            this.listView_Notes.TabIndex = 4;
            this.listView_Notes.UseCompatibleStateImageBehavior = false;
            this.listView_Notes.View = System.Windows.Forms.View.Details;
            this.listView_Notes.SelectedIndexChanged += new System.EventHandler(this.listView_Notes_SelectedIndexChanged);
            // 
            // tb_note
            // 
            this.tb_note.Location = new System.Drawing.Point(48, 113);
            this.tb_note.Multiline = true;
            this.tb_note.Name = "tb_note";
            this.tb_note.Size = new System.Drawing.Size(709, 88);
            this.tb_note.TabIndex = 5;
            // 
            // btn_mainwindow
            // 
            this.btn_mainwindow.Location = new System.Drawing.Point(697, 12);
            this.btn_mainwindow.Name = "btn_mainwindow";
            this.btn_mainwindow.Size = new System.Drawing.Size(91, 68);
            this.btn_mainwindow.TabIndex = 6;
            this.btn_mainwindow.Text = "Back to Main Window";
            this.btn_mainwindow.UseVisualStyleBackColor = true;
            this.btn_mainwindow.Click += new System.EventHandler(this.btn_mainwindow_Click);
            // 
            // NotesWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btn_mainwindow);
            this.Controls.Add(this.tb_note);
            this.Controls.Add(this.listView_Notes);
            this.Controls.Add(this.btn_delete);
            this.Controls.Add(this.btn_update);
            this.Controls.Add(this.btn_create);
            this.Controls.Add(this.btn_list);
            this.Name = "NotesWindow";
            this.Text = "NotesWindow";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.NotesWindow_FormClosed);
            this.Load += new System.EventHandler(this.NotesWindow_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_list;
        private System.Windows.Forms.Button btn_create;
        private System.Windows.Forms.Button btn_update;
        private System.Windows.Forms.Button btn_delete;
        private System.Windows.Forms.ListView listView_Notes;
        private System.Windows.Forms.TextBox tb_note;
        private System.Windows.Forms.Button btn_mainwindow;
    }
}