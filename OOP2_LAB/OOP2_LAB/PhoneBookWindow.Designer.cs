﻿namespace OOP2_LAB
{
    partial class PhoneBookWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_name = new System.Windows.Forms.Label();
            this.lbl_surname = new System.Windows.Forms.Label();
            this.lbl_phone_number = new System.Windows.Forms.Label();
            this.lbl_address = new System.Windows.Forms.Label();
            this.lbl_description = new System.Windows.Forms.Label();
            this.lbl_e_mail = new System.Windows.Forms.Label();
            this.tb_name = new System.Windows.Forms.TextBox();
            this.tb_surname = new System.Windows.Forms.TextBox();
            this.tb_address = new System.Windows.Forms.TextBox();
            this.tb_description = new System.Windows.Forms.TextBox();
            this.tb_e_mail = new System.Windows.Forms.TextBox();
            this.btn_new_contact = new System.Windows.Forms.Button();
            this.btn_all_list = new System.Windows.Forms.Button();
            this.listView_phone_book = new System.Windows.Forms.ListView();
            this.tb_phone_number = new System.Windows.Forms.MaskedTextBox();
            this.btn_update = new System.Windows.Forms.Button();
            this.btn_delete = new System.Windows.Forms.Button();
            this.btn_menu = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbl_name
            // 
            this.lbl_name.AutoSize = true;
            this.lbl_name.Location = new System.Drawing.Point(32, 39);
            this.lbl_name.Name = "lbl_name";
            this.lbl_name.Size = new System.Drawing.Size(53, 17);
            this.lbl_name.TabIndex = 0;
            this.lbl_name.Text = "Name :";
            // 
            // lbl_surname
            // 
            this.lbl_surname.AutoSize = true;
            this.lbl_surname.Location = new System.Drawing.Point(32, 81);
            this.lbl_surname.Name = "lbl_surname";
            this.lbl_surname.Size = new System.Drawing.Size(73, 17);
            this.lbl_surname.TabIndex = 1;
            this.lbl_surname.Text = "Surname :";
            // 
            // lbl_phone_number
            // 
            this.lbl_phone_number.AutoSize = true;
            this.lbl_phone_number.Location = new System.Drawing.Point(32, 123);
            this.lbl_phone_number.Name = "lbl_phone_number";
            this.lbl_phone_number.Size = new System.Drawing.Size(111, 17);
            this.lbl_phone_number.TabIndex = 2;
            this.lbl_phone_number.Text = "Phone Number :";
            // 
            // lbl_address
            // 
            this.lbl_address.AutoSize = true;
            this.lbl_address.Location = new System.Drawing.Point(32, 165);
            this.lbl_address.Name = "lbl_address";
            this.lbl_address.Size = new System.Drawing.Size(68, 17);
            this.lbl_address.TabIndex = 3;
            this.lbl_address.Text = "Address :";
            // 
            // lbl_description
            // 
            this.lbl_description.AutoSize = true;
            this.lbl_description.Location = new System.Drawing.Point(288, 81);
            this.lbl_description.Name = "lbl_description";
            this.lbl_description.Size = new System.Drawing.Size(87, 17);
            this.lbl_description.TabIndex = 4;
            this.lbl_description.Text = "Description :";
            // 
            // lbl_e_mail
            // 
            this.lbl_e_mail.AutoSize = true;
            this.lbl_e_mail.Location = new System.Drawing.Point(288, 39);
            this.lbl_e_mail.Name = "lbl_e_mail";
            this.lbl_e_mail.Size = new System.Drawing.Size(55, 17);
            this.lbl_e_mail.TabIndex = 5;
            this.lbl_e_mail.Text = "E-mail :";
            // 
            // tb_name
            // 
            this.tb_name.Location = new System.Drawing.Point(149, 36);
            this.tb_name.Name = "tb_name";
            this.tb_name.Size = new System.Drawing.Size(100, 22);
            this.tb_name.TabIndex = 6;
            // 
            // tb_surname
            // 
            this.tb_surname.Location = new System.Drawing.Point(149, 78);
            this.tb_surname.Name = "tb_surname";
            this.tb_surname.Size = new System.Drawing.Size(100, 22);
            this.tb_surname.TabIndex = 7;
            // 
            // tb_address
            // 
            this.tb_address.Location = new System.Drawing.Point(149, 162);
            this.tb_address.Name = "tb_address";
            this.tb_address.Size = new System.Drawing.Size(441, 22);
            this.tb_address.TabIndex = 9;
            // 
            // tb_description
            // 
            this.tb_description.Location = new System.Drawing.Point(405, 76);
            this.tb_description.Name = "tb_description";
            this.tb_description.Size = new System.Drawing.Size(185, 22);
            this.tb_description.TabIndex = 10;
            // 
            // tb_e_mail
            // 
            this.tb_e_mail.Location = new System.Drawing.Point(405, 36);
            this.tb_e_mail.Name = "tb_e_mail";
            this.tb_e_mail.Size = new System.Drawing.Size(185, 22);
            this.tb_e_mail.TabIndex = 11;
            this.tb_e_mail.Validating += new System.ComponentModel.CancelEventHandler(this.tb_e_mail_Validating);
            // 
            // btn_new_contact
            // 
            this.btn_new_contact.Location = new System.Drawing.Point(633, 33);
            this.btn_new_contact.Name = "btn_new_contact";
            this.btn_new_contact.Size = new System.Drawing.Size(119, 28);
            this.btn_new_contact.TabIndex = 12;
            this.btn_new_contact.Text = "New Contact";
            this.btn_new_contact.UseVisualStyleBackColor = true;
            this.btn_new_contact.Click += new System.EventHandler(this.btn_new_contact_Click);
            // 
            // btn_all_list
            // 
            this.btn_all_list.Location = new System.Drawing.Point(633, 73);
            this.btn_all_list.Name = "btn_all_list";
            this.btn_all_list.Size = new System.Drawing.Size(119, 28);
            this.btn_all_list.TabIndex = 13;
            this.btn_all_list.Text = "All List";
            this.btn_all_list.UseVisualStyleBackColor = true;
            this.btn_all_list.Click += new System.EventHandler(this.btn_all_list_Click);
            // 
            // listView_phone_book
            // 
            this.listView_phone_book.FullRowSelect = true;
            this.listView_phone_book.GridLines = true;
            this.listView_phone_book.HideSelection = false;
            this.listView_phone_book.Location = new System.Drawing.Point(35, 235);
            this.listView_phone_book.Name = "listView_phone_book";
            this.listView_phone_book.Size = new System.Drawing.Size(717, 141);
            this.listView_phone_book.TabIndex = 14;
            this.listView_phone_book.UseCompatibleStateImageBehavior = false;
            this.listView_phone_book.View = System.Windows.Forms.View.Details;
            this.listView_phone_book.SelectedIndexChanged += new System.EventHandler(this.listView_phone_book_SelectedIndexChanged);
            // 
            // tb_phone_number
            // 
            this.tb_phone_number.Location = new System.Drawing.Point(149, 120);
            this.tb_phone_number.Mask = "(999) 000-0000";
            this.tb_phone_number.Name = "tb_phone_number";
            this.tb_phone_number.Size = new System.Drawing.Size(100, 22);
            this.tb_phone_number.TabIndex = 15;
            // 
            // btn_update
            // 
            this.btn_update.Location = new System.Drawing.Point(633, 117);
            this.btn_update.Name = "btn_update";
            this.btn_update.Size = new System.Drawing.Size(119, 28);
            this.btn_update.TabIndex = 16;
            this.btn_update.Text = "Edit";
            this.btn_update.UseVisualStyleBackColor = true;
            this.btn_update.Click += new System.EventHandler(this.btn_update_Click);
            // 
            // btn_delete
            // 
            this.btn_delete.Location = new System.Drawing.Point(633, 159);
            this.btn_delete.Name = "btn_delete";
            this.btn_delete.Size = new System.Drawing.Size(119, 28);
            this.btn_delete.TabIndex = 17;
            this.btn_delete.Text = "Delete";
            this.btn_delete.UseVisualStyleBackColor = true;
            this.btn_delete.Click += new System.EventHandler(this.btn_delete_Click);
            // 
            // btn_menu
            // 
            this.btn_menu.Location = new System.Drawing.Point(633, 382);
            this.btn_menu.Name = "btn_menu";
            this.btn_menu.Size = new System.Drawing.Size(119, 56);
            this.btn_menu.TabIndex = 18;
            this.btn_menu.Text = "Back to Main Window";
            this.btn_menu.UseVisualStyleBackColor = true;
            this.btn_menu.Click += new System.EventHandler(this.btn_menu_Click);
            // 
            // PhoneBookWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btn_menu);
            this.Controls.Add(this.btn_delete);
            this.Controls.Add(this.btn_update);
            this.Controls.Add(this.tb_phone_number);
            this.Controls.Add(this.listView_phone_book);
            this.Controls.Add(this.btn_all_list);
            this.Controls.Add(this.btn_new_contact);
            this.Controls.Add(this.tb_e_mail);
            this.Controls.Add(this.tb_description);
            this.Controls.Add(this.tb_address);
            this.Controls.Add(this.tb_surname);
            this.Controls.Add(this.tb_name);
            this.Controls.Add(this.lbl_e_mail);
            this.Controls.Add(this.lbl_description);
            this.Controls.Add(this.lbl_address);
            this.Controls.Add(this.lbl_phone_number);
            this.Controls.Add(this.lbl_surname);
            this.Controls.Add(this.lbl_name);
            this.Name = "PhoneBookWindow";
            this.Text = "PhoneBookWindow";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.PhoneBookWindow_FormClosing);
            this.Load += new System.EventHandler(this.PhoneBookWindow_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_name;
        private System.Windows.Forms.Label lbl_surname;
        private System.Windows.Forms.Label lbl_phone_number;
        private System.Windows.Forms.Label lbl_address;
        private System.Windows.Forms.Label lbl_description;
        private System.Windows.Forms.Label lbl_e_mail;
        private System.Windows.Forms.TextBox tb_name;
        private System.Windows.Forms.TextBox tb_surname;
        private System.Windows.Forms.TextBox tb_address;
        private System.Windows.Forms.TextBox tb_description;
        private System.Windows.Forms.TextBox tb_e_mail;
        private System.Windows.Forms.Button btn_new_contact;
        private System.Windows.Forms.Button btn_all_list;
        private System.Windows.Forms.ListView listView_phone_book;
        private System.Windows.Forms.MaskedTextBox tb_phone_number;
        private System.Windows.Forms.Button btn_update;
        private System.Windows.Forms.Button btn_delete;
        private System.Windows.Forms.Button btn_menu;
    }
}