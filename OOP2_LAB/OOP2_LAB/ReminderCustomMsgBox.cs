﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OOP2_LAB
{
    public partial class ReminderCustomMsgBox : Form
    {
        public ReminderCustomMsgBox()
        {
            InitializeComponent();
        }
        public static int ShakeCount = 0;
        static ReminderCustomMsgBox MsgBox;
        static DialogResult result = DialogResult.OK;
        public static DialogResult Show(string text,string caption)
        {
            MsgBox = new ReminderCustomMsgBox();
            MsgBox.lbl_summary_txt.Text = text;
            MsgBox.Text = caption;
            MsgBox.ShowDialog();
            return result;
        }

        private void btn_OK_Click(object sender, EventArgs e)
        {
             result = DialogResult.OK;
             MsgBox.Close();
        }

        public static void Shake(Form form)
        {
            var original = form.Location;
            var rnd = new Random(1337);
            const int shake_amplitude = 10;
            for(int i=0;i<70;i++)
            {
                form.Location = new Point(original.X + rnd.Next(-shake_amplitude, shake_amplitude), original.Y + rnd.Next(-shake_amplitude, shake_amplitude));
                System.Threading.Thread.Sleep(20);
                form.Location = original;
            }
        }

        private void ReminderCustomMsgBox_Load(object sender, EventArgs e)
        {
            timer_CustomMsgBox.Start(); 
        }

        private void timer_CustomMsgBox_Tick(object sender, EventArgs e)
        {
            timer_CustomMsgBox.Stop();
            Shake(this);
        }
    }
}
