﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP2_LAB
{

    public class Salary
    {
        private double total_salary=5000.0;
        private double deneyim_katsayisi;
        private double yasanilan_il_katsayisi;
        private double ust_ogrenim_katsayisi;
        private double yabanci_dil_katsayisi;
        private double yoneticilik_gorevi_katsayisi;
        private double aile_durumu_katsayisi;
       
        public double total_Salary { get => total_salary; set => total_salary = value; }
        public double deneyim_Katsayisi { get => deneyim_katsayisi; set => deneyim_katsayisi = value; }
        public double yasanilan_İl_Katsayisi { get => yasanilan_il_katsayisi; set => yasanilan_il_katsayisi = value; }
        public double ust_Ogrenim_Katsayisi { get => ust_ogrenim_katsayisi; set => ust_ogrenim_katsayisi = value; }
        public double yabanci_Dil_Katsayisi { get => yabanci_dil_katsayisi; set => yabanci_dil_katsayisi = value; }
        public double yoneticilik_Gorevi_Katsayisi { get => yoneticilik_gorevi_katsayisi; set => yoneticilik_gorevi_katsayisi = value; }
        public double aile_Durumu_Katsayisi { get => aile_durumu_katsayisi; set => aile_durumu_katsayisi = value; }
       
    }
    public abstract class Salary_Builder
    {
        protected Salary _salary;
        public Salary Salary
        {
            get { return _salary; }
        }
        public abstract void maas_hesapla();
    }
    public class User_salary_builder : Salary_Builder
    {
        public User_salary_builder(Salary slry)
        {
            _salary = new Salary { total_Salary = 5000.0 };
            _salary.deneyim_Katsayisi = slry.deneyim_Katsayisi;
            _salary.yasanilan_İl_Katsayisi = slry.yasanilan_İl_Katsayisi;
            _salary.ust_Ogrenim_Katsayisi = slry.ust_Ogrenim_Katsayisi;
            _salary.yabanci_Dil_Katsayisi = slry.yabanci_Dil_Katsayisi;
            _salary.yoneticilik_Gorevi_Katsayisi = slry.yoneticilik_Gorevi_Katsayisi;
            _salary.aile_Durumu_Katsayisi = slry.aile_Durumu_Katsayisi;
        }
        public override void maas_hesapla()
        {
            _salary.total_Salary = _salary.total_Salary * (_salary.deneyim_Katsayisi +
                            _salary.yasanilan_İl_Katsayisi + _salary.ust_Ogrenim_Katsayisi + _salary.yabanci_Dil_Katsayisi +
                            _salary.yoneticilik_Gorevi_Katsayisi + _salary.aile_Durumu_Katsayisi + 1.0);
        }
       
    }
    public class Part_time_salary : Salary_Builder
    {
        public Part_time_salary(Salary slry)
        {
            _salary = new Salary { total_Salary = 5000.0 };
            _salary.deneyim_Katsayisi = slry.deneyim_Katsayisi;
            _salary.yasanilan_İl_Katsayisi = slry.yasanilan_İl_Katsayisi;
            _salary.ust_Ogrenim_Katsayisi = slry.ust_Ogrenim_Katsayisi;
            _salary.yabanci_Dil_Katsayisi = slry.yabanci_Dil_Katsayisi;
            _salary.yoneticilik_Gorevi_Katsayisi = slry.yoneticilik_Gorevi_Katsayisi;
            _salary.aile_Durumu_Katsayisi = slry.aile_Durumu_Katsayisi;
        }
        public override void maas_hesapla()
        {
            _salary.total_Salary = (_salary.total_Salary * (_salary.deneyim_Katsayisi +
                            _salary.yasanilan_İl_Katsayisi + _salary.ust_Ogrenim_Katsayisi + _salary.yabanci_Dil_Katsayisi +
                            _salary.yoneticilik_Gorevi_Katsayisi + _salary.aile_Durumu_Katsayisi + 1.0)) / 2.0;
        }
       
    }
}
