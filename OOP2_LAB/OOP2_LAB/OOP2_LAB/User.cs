﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP2_LAB
{
    public class User
    {
        private String username;
        private String password;
        private bool rememberMe;
        private int userType;
        public List<PhoneBook> userPhoneBook = new List<PhoneBook>();
        public List<Notes> userNotes = new List<Notes>();
        public PersonalInformation userPI;
        public User(String username, String password, bool rememberMe, int userType, PersonalInformation userPI)
        {
            this.username = username;
            this.password = password;
            this.rememberMe = rememberMe;
            this.userType = userType;
            this.userPI = userPI;
        }

        public string Username { get => username; set => username = value; }
        public string Password { get => password; set => password = value; }
        public bool RememberMe { get => rememberMe; set => rememberMe = value; }

        public int UserType { get => userType; set => userType = value; }

        public bool IsValid(string username, string password)
        {
            return this.username.Equals(username) && this.password.Equals(Util.ComputeSha256Hash(password));
        }

        public string ToString()
        {
            return Username + "," + Password + "," + (RememberMe ? "1" : "0") + "," + userType.ToString() + "," + userPI.ToString();
        }
    }
}
