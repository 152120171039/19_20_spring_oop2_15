﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP2_LAB
{
    public class LoginedUser
    {
        private User user;

        private static LoginedUser loginedUser;
        public static LoginedUser getInstance()
        {
            if (loginedUser == null)
            {
                loginedUser = new LoginedUser();
            }
            return loginedUser;
        }
        public User User { get => user; set => user = value; }
    }
}
