﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP2_LAB
{
    public class PersonalInformation
    {
        private string name;
        private string surname;
        private string phoneNumber;
        private string address;
        private string mail;
        private string photoBase64;
        private string _salary;

        public PersonalInformation()
        {
            name = "Name";
            surname = "Surname";
            phoneNumber = "(999) 999-9999";
            address = "Address";
            mail = "mail@mail.com";
            photoBase64 = "No photo";
            _salary = "0.0";
        }

        public PersonalInformation(string name, string surname, string phoneNumber, string address, string mail,string photoBase64,string _salary)
        {
            this.name = name;
            this.surname = surname;
            this.phoneNumber = phoneNumber;
            this.address = address;
            this.mail = mail;
            this.photoBase64 = photoBase64;
            this._salary = _salary;
        }

        public string Name { get => name; set => name = value; }
        public string Surname { get => surname; set => surname = value; }
        public string PhoneNumber { get => phoneNumber; set => phoneNumber = value; }
        public string Address { get => address; set => address = value; }
        public string Mail { get => mail; set => mail = value; }
        public string PhotoBase64 { get => photoBase64; set => photoBase64 = value; }
        public string Salary_ { get => _salary; set => _salary = value; }
        public string ToString()
        {
            return Name + "," + Surname + "," + PhoneNumber + "," + Address + "," + Mail + "," + PhotoBase64 + "," + Salary_;
        }
    }
}
