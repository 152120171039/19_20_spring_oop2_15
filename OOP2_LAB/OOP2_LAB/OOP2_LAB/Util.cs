﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.IO;
using System.Text.RegularExpressions;

namespace OOP2_LAB
{
    public static class Util
    {
        public static string ComputeSha256Hash(string rawData)
        {
            // Create a SHA256   
            using (SHA256 sha256Hash = SHA256.Create())
            {
                // ComputeHash - returns byte array  
                byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(rawData));

                // Convert byte array to a string   
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("x2"));
                }
                return builder.ToString();
            }
        }

        public static void LoadCsv(List<User> userlist, string path)
        {
            if (!File.Exists(path))
            {
                var newUserFile = File.Create(path);
                newUserFile.Close();
            }
            using (var reader = new StreamReader(path))
            {
                if (reader == null)
                {
                    File.Create(path);
                }
                while (!reader.EndOfStream)
                {
                    string line = reader.ReadLine();
                    string[] values = line.Split(',');
                    string username = values[0];
                    string password = values[1];
                    bool rememberMe = values[2].Equals("1")?true:false;
                    int userType = Int32.Parse(values[3]);
                    string name = values[4];
                    string surname = values[5];
                    string phoneNumber = values[6];
                    string address = values[7];
                    string mail = values[8];
                    string photobase64 = values[9];
                    string salary = values[10];
                    PersonalInformation newPI = new PersonalInformation(name, surname, phoneNumber, address, mail, photobase64,salary);
                    userlist.Add(new User(username, password, rememberMe, userType,newPI));
                }
            }
        }

        public static void SaveCsv(List<User> userlist, string path)
        {
            using (var writer = new StreamWriter(path))
            {
                foreach (User user in userlist)
                {
                    writer.WriteLine(user.ToString());
                }
            }
        }
        public static void LoadCsv(string path)
        {
            if (!File.Exists(path))
            {
                var newPhoneBookFile = File.Create(path);
                newPhoneBookFile.Close();
            }
            using (var reader = new StreamReader(path))
            {
                if (reader == null)
                {
                    File.Create(path);
                }
                while (!reader.EndOfStream)
                {
                    string line = reader.ReadLine();
                    string[] values = line.Split(',');
                    string name = values[0];
                    string surname = values[1];
                    string phone_number = values[2];
                    string address = values[3];
                    string description = values[4];
                    string e_mail = values[5];
                    string whoEntered = values[6];
                    for (int i = 0; i < LoginWindow.userList.Count; i++)
                    {
                        if (LoginWindow.userList[i].Username == whoEntered)
                        {
                            LoginWindow.userList[i].userPhoneBook.Add(new PhoneBook(name, surname, phone_number, address, description, e_mail, whoEntered));
                            break;
                        }
                    }
                   
                    //PhoneBookList.Add(new PhoneBook(name,surname,phone_number,address,description,e_mail,whoEntered));
                }
            }
        }
        public static void SaveCsv(List<PhoneBook> PhoneBookList, string path)
        {
            using (var writer = new StreamWriter(path))
            {
                foreach (PhoneBook Phone_book in PhoneBookList)
                {
                    writer.WriteLine(Phone_book.ToString_save());
                }
            }
        }
        public static void Delete_line(List<PhoneBook> PhoneBookList, string path)
        {
            List<string> newlist = File.ReadAllLines(path).ToList();
            newlist.RemoveAt(PhoneBookWindow.selected_index);
            File.WriteAllLines(path, newlist.ToArray());
        }

        public static void Delete_line(List<Notes> NoteList, string path)
        {
            List<string> newlist = File.ReadAllLines(path).ToList();
            newlist.RemoveAt(NotesWindow.selected_index);
            File.WriteAllLines(path, newlist.ToArray());
        }

        public static void LoadNotesCsv(string path)
        {
            if (!File.Exists(path))
            {
                var newNotesFile = File.Create(path);
                newNotesFile.Close();
            }
            using (var reader = new StreamReader(path))
            {
                if (reader == null)
                {
                    File.Create(path);
                }
                while (!reader.EndOfStream)
                {
                    string line = reader.ReadLine();
                    string[] values = line.Split(',');
                    string note = values[0];
                    string whoEntered = values[1];

                    for (int i = 0; i < LoginWindow.userList.Count; i++)
                    {
                        if (LoginWindow.userList[i].Username == whoEntered)
                        {
                            if(note.Contains("\\n"))
                            {
                                string not = note.Replace("\\n",Environment.NewLine);
                                note = not;                                
                            }
                            LoginWindow.userList[i].userNotes.Add(new Notes(note, whoEntered));
                            break;
                        }
                    }
                }
            }
        }

        public static bool IsNoteSpace(string note)
        {
            Regex regex = new Regex(@"\n+");
            return regex.IsMatch(note);
        }
        

        public static void SaveCsv(List<Notes> NotesList, string path)
        {
            using (var writer = new StreamWriter(path))
            {
                foreach (Notes Note in NotesList)
                {
                    if(IsNoteSpace(Note.Note)==true)
                    {
                        writer.WriteLine(Note.Note.Replace(System.Environment.NewLine, "\\n")+","+Note.WhoEntered);
                    }
                }
            }
        }
    }
}
