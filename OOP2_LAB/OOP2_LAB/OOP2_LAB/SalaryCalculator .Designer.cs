﻿namespace OOP2_LAB
{
    partial class SalaryCalculator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.combo_box_deneyim = new System.Windows.Forms.ComboBox();
            this.lbl_deneyim = new System.Windows.Forms.Label();
            this.lbl_yasalinan_il = new System.Windows.Forms.Label();
            this.combo_Box_yasalinan_il = new System.Windows.Forms.ComboBox();
            this.btn_calculater = new System.Windows.Forms.Button();
            this.lbl_Akademik_Derece = new System.Windows.Forms.Label();
            this.lbl_Yabancı_Dil_Bilgisi = new System.Windows.Forms.Label();
            this.lbl_Yöneticilik_Görevi = new System.Windows.Forms.Label();
            this.combo_Box_Akademik_Derece = new System.Windows.Forms.ComboBox();
            this.combo_Box_Yöneticilik_Görevi = new System.Windows.Forms.ComboBox();
            this.checkBox_yabancı_dil = new System.Windows.Forms.CheckBox();
            this.list_box_ekstra_diller = new System.Windows.Forms.ListBox();
            this.textBox_yabancı_dil_ekle = new System.Windows.Forms.TextBox();
            this.btn_yabancı_dil_ekle = new System.Windows.Forms.Button();
            this.groupBox_aile = new System.Windows.Forms.GroupBox();
            this.lbl_acıklama_aile = new System.Windows.Forms.Label();
            this.checkBox_2cocuk = new System.Windows.Forms.CheckBox();
            this.checkBox_18yas = new System.Windows.Forms.CheckBox();
            this.checkBox_7_18yas = new System.Windows.Forms.CheckBox();
            this.checkBox_0_6yaş = new System.Windows.Forms.CheckBox();
            this.checkBox_çalışmıyor = new System.Windows.Forms.CheckBox();
            this.checkBox_belgelendirilmiş_ing = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.groupBox_yabancı_dil = new System.Windows.Forms.GroupBox();
            this.checkBox_ing_okul = new System.Windows.Forms.CheckBox();
            this.btn_mainwindow = new System.Windows.Forms.Button();
            this.groupBox_aile.SuspendLayout();
            this.groupBox_yabancı_dil.SuspendLayout();
            this.SuspendLayout();
            // 
            // combo_box_deneyim
            // 
            this.combo_box_deneyim.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.combo_box_deneyim.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.combo_box_deneyim.FormattingEnabled = true;
            this.combo_box_deneyim.Location = new System.Drawing.Point(193, 72);
            this.combo_box_deneyim.Name = "combo_box_deneyim";
            this.combo_box_deneyim.Size = new System.Drawing.Size(121, 24);
            this.combo_box_deneyim.TabIndex = 0;
            this.combo_box_deneyim.SelectedIndexChanged += new System.EventHandler(this.combo_box_deneyim_SelectedIndexChanged);
            // 
            // lbl_deneyim
            // 
            this.lbl_deneyim.AutoSize = true;
            this.lbl_deneyim.Location = new System.Drawing.Point(49, 74);
            this.lbl_deneyim.Name = "lbl_deneyim";
            this.lbl_deneyim.Size = new System.Drawing.Size(144, 17);
            this.lbl_deneyim.TabIndex = 1;
            this.lbl_deneyim.Text = "Deneyim Süresi (Yıl ):";
            // 
            // lbl_yasalinan_il
            // 
            this.lbl_yasalinan_il.AutoSize = true;
            this.lbl_yasalinan_il.Location = new System.Drawing.Point(372, 75);
            this.lbl_yasalinan_il.Name = "lbl_yasalinan_il";
            this.lbl_yasalinan_il.Size = new System.Drawing.Size(132, 17);
            this.lbl_yasalinan_il.TabIndex = 2;
            this.lbl_yasalinan_il.Text = "Yaşanılan İl Grubu :";
            // 
            // combo_Box_yasalinan_il
            // 
            this.combo_Box_yasalinan_il.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.combo_Box_yasalinan_il.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.combo_Box_yasalinan_il.FormattingEnabled = true;
            this.combo_Box_yasalinan_il.Location = new System.Drawing.Point(516, 72);
            this.combo_Box_yasalinan_il.Name = "combo_Box_yasalinan_il";
            this.combo_Box_yasalinan_il.Size = new System.Drawing.Size(121, 24);
            this.combo_Box_yasalinan_il.TabIndex = 3;
            this.combo_Box_yasalinan_il.SelectedIndexChanged += new System.EventHandler(this.combo_Box_yasalinan_il_SelectedIndexChanged);
            // 
            // btn_calculater
            // 
            this.btn_calculater.Location = new System.Drawing.Point(757, 455);
            this.btn_calculater.Name = "btn_calculater";
            this.btn_calculater.Size = new System.Drawing.Size(106, 48);
            this.btn_calculater.TabIndex = 6;
            this.btn_calculater.Text = "Calculate";
            this.btn_calculater.UseVisualStyleBackColor = true;
            this.btn_calculater.Click += new System.EventHandler(this.btn_calculater_Click);
            // 
            // lbl_Akademik_Derece
            // 
            this.lbl_Akademik_Derece.AutoSize = true;
            this.lbl_Akademik_Derece.Location = new System.Drawing.Point(49, 114);
            this.lbl_Akademik_Derece.Name = "lbl_Akademik_Derece";
            this.lbl_Akademik_Derece.Size = new System.Drawing.Size(127, 17);
            this.lbl_Akademik_Derece.TabIndex = 7;
            this.lbl_Akademik_Derece.Text = "Akademik Derece :";
            // 
            // lbl_Yabancı_Dil_Bilgisi
            // 
            this.lbl_Yabancı_Dil_Bilgisi.AutoSize = true;
            this.lbl_Yabancı_Dil_Bilgisi.Location = new System.Drawing.Point(49, 259);
            this.lbl_Yabancı_Dil_Bilgisi.Name = "lbl_Yabancı_Dil_Bilgisi";
            this.lbl_Yabancı_Dil_Bilgisi.Size = new System.Drawing.Size(127, 17);
            this.lbl_Yabancı_Dil_Bilgisi.TabIndex = 8;
            this.lbl_Yabancı_Dil_Bilgisi.Text = "Yabancı Dil Bilgisi :";
            // 
            // lbl_Yöneticilik_Görevi
            // 
            this.lbl_Yöneticilik_Görevi.AutoSize = true;
            this.lbl_Yöneticilik_Görevi.Location = new System.Drawing.Point(49, 159);
            this.lbl_Yöneticilik_Görevi.Name = "lbl_Yöneticilik_Görevi";
            this.lbl_Yöneticilik_Görevi.Size = new System.Drawing.Size(125, 17);
            this.lbl_Yöneticilik_Görevi.TabIndex = 9;
            this.lbl_Yöneticilik_Görevi.Text = "Yöneticilik Görevi :";
            // 
            // combo_Box_Akademik_Derece
            // 
            this.combo_Box_Akademik_Derece.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.combo_Box_Akademik_Derece.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.combo_Box_Akademik_Derece.FormattingEnabled = true;
            this.combo_Box_Akademik_Derece.Location = new System.Drawing.Point(193, 111);
            this.combo_Box_Akademik_Derece.Name = "combo_Box_Akademik_Derece";
            this.combo_Box_Akademik_Derece.Size = new System.Drawing.Size(444, 24);
            this.combo_Box_Akademik_Derece.TabIndex = 11;
            this.combo_Box_Akademik_Derece.SelectedIndexChanged += new System.EventHandler(this.combo_Box_Akademik_Derece_SelectedIndexChanged);
            // 
            // combo_Box_Yöneticilik_Görevi
            // 
            this.combo_Box_Yöneticilik_Görevi.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.combo_Box_Yöneticilik_Görevi.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.combo_Box_Yöneticilik_Görevi.FormattingEnabled = true;
            this.combo_Box_Yöneticilik_Görevi.Location = new System.Drawing.Point(193, 156);
            this.combo_Box_Yöneticilik_Görevi.Name = "combo_Box_Yöneticilik_Görevi";
            this.combo_Box_Yöneticilik_Görevi.Size = new System.Drawing.Size(444, 24);
            this.combo_Box_Yöneticilik_Görevi.TabIndex = 13;
            this.combo_Box_Yöneticilik_Görevi.SelectedIndexChanged += new System.EventHandler(this.combo_Box_Yöneticilik_Görevi_SelectedIndexChanged);
            // 
            // checkBox_yabancı_dil
            // 
            this.checkBox_yabancı_dil.AutoSize = true;
            this.checkBox_yabancı_dil.Location = new System.Drawing.Point(6, 88);
            this.checkBox_yabancı_dil.Name = "checkBox_yabancı_dil";
            this.checkBox_yabancı_dil.Size = new System.Drawing.Size(226, 21);
            this.checkBox_yabancı_dil.TabIndex = 15;
            this.checkBox_yabancı_dil.Text = "İngilizce dışında bilinen dil/diller";
            this.checkBox_yabancı_dil.UseVisualStyleBackColor = true;
            this.checkBox_yabancı_dil.CheckedChanged += new System.EventHandler(this.checkBox_yabancı_dil_CheckedChanged);
            // 
            // list_box_ekstra_diller
            // 
            this.list_box_ekstra_diller.FormattingEnabled = true;
            this.list_box_ekstra_diller.ItemHeight = 16;
            this.list_box_ekstra_diller.Location = new System.Drawing.Point(670, 229);
            this.list_box_ekstra_diller.Name = "list_box_ekstra_diller";
            this.list_box_ekstra_diller.Size = new System.Drawing.Size(120, 84);
            this.list_box_ekstra_diller.TabIndex = 16;
            // 
            // textBox_yabancı_dil_ekle
            // 
            this.textBox_yabancı_dil_ekle.Location = new System.Drawing.Point(527, 229);
            this.textBox_yabancı_dil_ekle.Name = "textBox_yabancı_dil_ekle";
            this.textBox_yabancı_dil_ekle.Size = new System.Drawing.Size(100, 22);
            this.textBox_yabancı_dil_ekle.TabIndex = 17;
            // 
            // btn_yabancı_dil_ekle
            // 
            this.btn_yabancı_dil_ekle.Location = new System.Drawing.Point(547, 268);
            this.btn_yabancı_dil_ekle.Name = "btn_yabancı_dil_ekle";
            this.btn_yabancı_dil_ekle.Size = new System.Drawing.Size(80, 23);
            this.btn_yabancı_dil_ekle.TabIndex = 18;
            this.btn_yabancı_dil_ekle.Text = "Ekle";
            this.btn_yabancı_dil_ekle.UseVisualStyleBackColor = true;
            this.btn_yabancı_dil_ekle.Click += new System.EventHandler(this.btn_yabancı_dil_ekle_Click);
            // 
            // groupBox_aile
            // 
            this.groupBox_aile.Controls.Add(this.lbl_acıklama_aile);
            this.groupBox_aile.Controls.Add(this.checkBox_2cocuk);
            this.groupBox_aile.Controls.Add(this.checkBox_18yas);
            this.groupBox_aile.Controls.Add(this.checkBox_7_18yas);
            this.groupBox_aile.Controls.Add(this.checkBox_0_6yaş);
            this.groupBox_aile.Controls.Add(this.checkBox_çalışmıyor);
            this.groupBox_aile.Location = new System.Drawing.Point(52, 353);
            this.groupBox_aile.Name = "groupBox_aile";
            this.groupBox_aile.Size = new System.Drawing.Size(327, 238);
            this.groupBox_aile.TabIndex = 19;
            this.groupBox_aile.TabStop = false;
            this.groupBox_aile.Text = "Aile durumu";
            // 
            // lbl_acıklama_aile
            // 
            this.lbl_acıklama_aile.AutoSize = true;
            this.lbl_acıklama_aile.Location = new System.Drawing.Point(22, 140);
            this.lbl_acıklama_aile.Name = "lbl_acıklama_aile";
            this.lbl_acıklama_aile.Size = new System.Drawing.Size(234, 34);
            this.lbl_acıklama_aile.TabIndex = 8;
            this.lbl_acıklama_aile.Text = "(Üniversite lisans /\r\nön lisans öğrencisi olmak koşuluyla)";
            // 
            // checkBox_2cocuk
            // 
            this.checkBox_2cocuk.AutoSize = true;
            this.checkBox_2cocuk.Location = new System.Drawing.Point(166, 191);
            this.checkBox_2cocuk.Name = "checkBox_2cocuk";
            this.checkBox_2cocuk.Size = new System.Drawing.Size(159, 38);
            this.checkBox_2cocuk.TabIndex = 5;
            this.checkBox_2cocuk.Text = "Aynı duruma ait\r\n 2.çocuk için seçiniz.\r\n";
            this.checkBox_2cocuk.UseVisualStyleBackColor = true;
            this.checkBox_2cocuk.CheckedChanged += new System.EventHandler(this.checkBox_2cocuk_CheckedChanged);
            // 
            // checkBox_18yas
            // 
            this.checkBox_18yas.AutoSize = true;
            this.checkBox_18yas.Location = new System.Drawing.Point(6, 117);
            this.checkBox_18yas.Name = "checkBox_18yas";
            this.checkBox_18yas.Size = new System.Drawing.Size(144, 21);
            this.checkBox_18yas.TabIndex = 3;
            this.checkBox_18yas.Text = "18 yaş üstü çocuk";
            this.checkBox_18yas.UseVisualStyleBackColor = true;
            this.checkBox_18yas.CheckedChanged += new System.EventHandler(this.checkBox_18yas_CheckedChanged);
            // 
            // checkBox_7_18yas
            // 
            this.checkBox_7_18yas.AutoSize = true;
            this.checkBox_7_18yas.Location = new System.Drawing.Point(6, 90);
            this.checkBox_7_18yas.Name = "checkBox_7_18yas";
            this.checkBox_7_18yas.Size = new System.Drawing.Size(165, 21);
            this.checkBox_7_18yas.TabIndex = 2;
            this.checkBox_7_18yas.Text = "7-18 yaş arası çocuk ";
            this.checkBox_7_18yas.UseVisualStyleBackColor = true;
            this.checkBox_7_18yas.CheckedChanged += new System.EventHandler(this.checkBox_7_18yas_CheckedChanged);
            // 
            // checkBox_0_6yaş
            // 
            this.checkBox_0_6yaş.AutoSize = true;
            this.checkBox_0_6yaş.Location = new System.Drawing.Point(6, 63);
            this.checkBox_0_6yaş.Name = "checkBox_0_6yaş";
            this.checkBox_0_6yaş.Size = new System.Drawing.Size(153, 21);
            this.checkBox_0_6yaş.TabIndex = 1;
            this.checkBox_0_6yaş.Text = "0-6 yaş arası çocuk";
            this.checkBox_0_6yaş.UseVisualStyleBackColor = true;
            this.checkBox_0_6yaş.CheckedChanged += new System.EventHandler(this.checkBox_0_6yaş_CheckedChanged);
            // 
            // checkBox_çalışmıyor
            // 
            this.checkBox_çalışmıyor.AutoSize = true;
            this.checkBox_çalışmıyor.Location = new System.Drawing.Point(6, 36);
            this.checkBox_çalışmıyor.Name = "checkBox_çalışmıyor";
            this.checkBox_çalışmıyor.Size = new System.Drawing.Size(159, 21);
            this.checkBox_çalışmıyor.TabIndex = 0;
            this.checkBox_çalışmıyor.Text = "Evli ve eşi çalışmıyor";
            this.checkBox_çalışmıyor.UseVisualStyleBackColor = true;
            this.checkBox_çalışmıyor.CheckedChanged += new System.EventHandler(this.checkBox_çalışmıyor_CheckedChanged);
            // 
            // checkBox_belgelendirilmiş_ing
            // 
            this.checkBox_belgelendirilmiş_ing.AutoSize = true;
            this.checkBox_belgelendirilmiş_ing.Location = new System.Drawing.Point(6, 34);
            this.checkBox_belgelendirilmiş_ing.Name = "checkBox_belgelendirilmiş_ing";
            this.checkBox_belgelendirilmiş_ing.Size = new System.Drawing.Size(225, 21);
            this.checkBox_belgelendirilmiş_ing.TabIndex = 20;
            this.checkBox_belgelendirilmiş_ing.Text = "Belgelendirilmiş İngilizce bilgisi ";
            this.checkBox_belgelendirilmiş_ing.UseVisualStyleBackColor = true;
            this.checkBox_belgelendirilmiş_ing.CheckedChanged += new System.EventHandler(this.checkBox_belgelendirilmiş_ing_CheckedChanged);
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(128, 34);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(98, 21);
            this.checkBox2.TabIndex = 21;
            this.checkBox2.Text = "checkBox2";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // groupBox_yabancı_dil
            // 
            this.groupBox_yabancı_dil.Controls.Add(this.checkBox_ing_okul);
            this.groupBox_yabancı_dil.Controls.Add(this.checkBox_belgelendirilmiş_ing);
            this.groupBox_yabancı_dil.Controls.Add(this.checkBox2);
            this.groupBox_yabancı_dil.Controls.Add(this.checkBox_yabancı_dil);
            this.groupBox_yabancı_dil.Location = new System.Drawing.Point(182, 204);
            this.groupBox_yabancı_dil.Name = "groupBox_yabancı_dil";
            this.groupBox_yabancı_dil.Size = new System.Drawing.Size(295, 133);
            this.groupBox_yabancı_dil.TabIndex = 22;
            this.groupBox_yabancı_dil.TabStop = false;
            this.groupBox_yabancı_dil.Text = "Yabancı Dil Bilgisi";
            // 
            // checkBox_ing_okul
            // 
            this.checkBox_ing_okul.AutoSize = true;
            this.checkBox_ing_okul.Location = new System.Drawing.Point(6, 61);
            this.checkBox_ing_okul.Name = "checkBox_ing_okul";
            this.checkBox_ing_okul.Size = new System.Drawing.Size(262, 21);
            this.checkBox_ing_okul.TabIndex = 22;
            this.checkBox_ing_okul.Text = "İngilizce eğitim veren okul mezuniyeti";
            this.checkBox_ing_okul.UseVisualStyleBackColor = true;
            this.checkBox_ing_okul.CheckedChanged += new System.EventHandler(this.checkBox_ing_okul_CheckedChanged);
            // 
            // btn_mainwindow
            // 
            this.btn_mainwindow.Location = new System.Drawing.Point(757, 531);
            this.btn_mainwindow.Name = "btn_mainwindow";
            this.btn_mainwindow.Size = new System.Drawing.Size(106, 43);
            this.btn_mainwindow.TabIndex = 23;
            this.btn_mainwindow.Text = "Back to\r\n Main Window";
            this.btn_mainwindow.UseVisualStyleBackColor = true;
            this.btn_mainwindow.Click += new System.EventHandler(this.btn_mainwindow_Click);
            // 
            // SalaryCalculator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(932, 615);
            this.Controls.Add(this.btn_mainwindow);
            this.Controls.Add(this.groupBox_yabancı_dil);
            this.Controls.Add(this.groupBox_aile);
            this.Controls.Add(this.btn_yabancı_dil_ekle);
            this.Controls.Add(this.textBox_yabancı_dil_ekle);
            this.Controls.Add(this.list_box_ekstra_diller);
            this.Controls.Add(this.combo_Box_Yöneticilik_Görevi);
            this.Controls.Add(this.combo_Box_Akademik_Derece);
            this.Controls.Add(this.lbl_Yöneticilik_Görevi);
            this.Controls.Add(this.lbl_Yabancı_Dil_Bilgisi);
            this.Controls.Add(this.lbl_Akademik_Derece);
            this.Controls.Add(this.btn_calculater);
            this.Controls.Add(this.combo_Box_yasalinan_il);
            this.Controls.Add(this.lbl_yasalinan_il);
            this.Controls.Add(this.lbl_deneyim);
            this.Controls.Add(this.combo_box_deneyim);
            this.Name = "SalaryCalculator";
            this.Text = "SalaryCalculator";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SalaryCalculator_FormClosing);
            this.Load += new System.EventHandler(this.SalaryCalculator_Load);
            this.groupBox_aile.ResumeLayout(false);
            this.groupBox_aile.PerformLayout();
            this.groupBox_yabancı_dil.ResumeLayout(false);
            this.groupBox_yabancı_dil.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox combo_box_deneyim;
        private System.Windows.Forms.Label lbl_deneyim;
        private System.Windows.Forms.Label lbl_yasalinan_il;
        private System.Windows.Forms.ComboBox combo_Box_yasalinan_il;
        private System.Windows.Forms.Button btn_calculater;
        private System.Windows.Forms.Label lbl_Akademik_Derece;
        private System.Windows.Forms.Label lbl_Yabancı_Dil_Bilgisi;
        private System.Windows.Forms.Label lbl_Yöneticilik_Görevi;
        private System.Windows.Forms.ComboBox combo_Box_Akademik_Derece;
        private System.Windows.Forms.ComboBox combo_Box_Yöneticilik_Görevi;
        private System.Windows.Forms.CheckBox checkBox_yabancı_dil;
        private System.Windows.Forms.ListBox list_box_ekstra_diller;
        private System.Windows.Forms.TextBox textBox_yabancı_dil_ekle;
        private System.Windows.Forms.Button btn_yabancı_dil_ekle;
        private System.Windows.Forms.GroupBox groupBox_aile;
        private System.Windows.Forms.CheckBox checkBox_18yas;
        private System.Windows.Forms.CheckBox checkBox_7_18yas;
        private System.Windows.Forms.CheckBox checkBox_0_6yaş;
        private System.Windows.Forms.CheckBox checkBox_çalışmıyor;
        private System.Windows.Forms.Label lbl_acıklama_aile;
        private System.Windows.Forms.CheckBox checkBox_2cocuk;
        private System.Windows.Forms.CheckBox checkBox_belgelendirilmiş_ing;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.GroupBox groupBox_yabancı_dil;
        private System.Windows.Forms.CheckBox checkBox_ing_okul;
        private System.Windows.Forms.Button btn_mainwindow;
    }
}