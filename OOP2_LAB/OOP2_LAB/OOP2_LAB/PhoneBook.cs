﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP2_LAB
{
   public class PhoneBook
    {
        private String name;
        private String surname;
        private String phone_number;
        private String address;
        private String description;
        private String e_mail;
        private String whoEntered;
        public PhoneBook(String name, String surname ,String phone_number,String address,String description, String e_mail,String whoEntered)
        {
            this.name = name;
            this.surname = surname;
            this.phone_number = phone_number;
            this.address = address;
            this.description = description;
            this.e_mail = e_mail;
            this.whoEntered = whoEntered;
        }

        public string Name { get => name; set => name = value; }
        public string Surname { get => surname; set => surname = value; }
        public string Phone_number { get => phone_number; set => phone_number = value; }
        public string Address { get => address; set => address = value; }
        public string Description { get => description; set => description = value; }
        public string E_mail { get => e_mail; set => e_mail = value; }

        public string WhoEntered { get => whoEntered; set => whoEntered = value; }
        public bool IsValid(string name,string surname,string phone_number,string address,string description,string e_mail)
        {
            return this.name.Equals(name) && this.surname.Equals(surname) && this.phone_number.Equals(phone_number) && this.address.Equals(address) && this.e_mail.Equals(e_mail);
        }
        public string ToString_save()
        {
            return Name + "," + Surname + "," + Phone_number + "," + Address + "," + Description + "," + E_mail + "," + WhoEntered;
        }
        public string add_contact()
        {
            return Name + Surname + Phone_number + Address + Description + E_mail;
        }

    }
}
