﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OOP2_LAB
{
    public partial class SalaryCalculator : Form
    {
        public SalaryCalculator()
        {
            InitializeComponent();
        }
        public string[] deneyim_suresi = { "2-4", "5-9", "10-14", "15-20", "20+","-" };

        public string[] iller = { "İstanbul","Ankara", "İzmir","Kocaeli, Sakarya, Düzce, Bolu, Yalova",
                                  "Edirne, Kırklareli, Tekirdağ","Trabzon, Ordu, Giresun, Rize, Artvin, Gümüşhane",
                                  "Bursa, Eskişehir, Bilecik","Aydın, Denizli, Muğla","Adana, Mersin",
                                  "Balıkesir, Çanakkale","Antalya, Isparta, Burdur","Diğer İller"};

        public string[] akademik_derece = { "Meslek alanı ile ilgili yüksek lisans", "Meslek alanı ile ilgili doktora",
                                            "Doçentlik", "Meslek alanı ile ilgili olmayan yüksek lisans",
                                            "Meslek alanı ile ilgili olmayan doktora/doçentlik","-" };

        public string[] Yöneticilik_Görevi = {  "Takım Lideri/Grup Yöneticisi/Teknik Yönetici/Yazılım Mimarı",
                                                "Proje Yöneticisi","Direktör/Projeler Yöneticisi","CTO/Genel Müdür",
                                                "Bilgi İşlem Sorumlusu/Müdürü (En çok 5 personel varsa)",
                                                "Bilgi İşlem Sorumlusu/Müdürü (5'ten çok personel varsa)","-"};
        public string[] yabancı_dil = { "Belgelendirilmiş İngilizce bilgisi", "İngilizce eğitim veren okul mezuniyeti","-" };

        public double kat_sayi= 0.0;
        public static double total_aile = 0.0;
        public static double total_dil = 0.0;
        public string whoEntered;
        Salary slry = new Salary();
        public static double result_salary = 0.0;
        private void SalaryCalculator_Load(object sender, EventArgs e)
        {
            combo_box_deneyim.Items.AddRange(deneyim_suresi);
            combo_Box_yasalinan_il.Items.AddRange(iller);
            combo_Box_Akademik_Derece.Items.AddRange(akademik_derece);
            combo_Box_Yöneticilik_Görevi.Items.AddRange(Yöneticilik_Görevi);
            btn_yabancı_dil_ekle.Hide();
            textBox_yabancı_dil_ekle.Hide();
            list_box_ekstra_diller.Hide();
        }
        private void combo_box_deneyim_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (combo_box_deneyim.SelectedItem.ToString()==deneyim_suresi[0].ToString())
            {
                kat_sayi = 0.60;
            }
            if (combo_box_deneyim.SelectedItem.ToString() == deneyim_suresi[1].ToString())
            {
                kat_sayi = 1.0;
            }
            if (combo_box_deneyim.SelectedItem.ToString() == deneyim_suresi[2].ToString())
            {
                kat_sayi = 1.20;
            }
            if (combo_box_deneyim.SelectedItem.ToString() == deneyim_suresi[3].ToString())
            {
                kat_sayi = 1.35;
            }
            if (combo_box_deneyim.SelectedItem.ToString() == deneyim_suresi[4].ToString())
            {
                kat_sayi = 1.50;
            }
            if (combo_box_deneyim.SelectedItem.ToString() == deneyim_suresi[5].ToString())
            {
                kat_sayi = 0.0;
            }
            slry.deneyim_Katsayisi = kat_sayi;
        }
        private void combo_Box_yasalinan_il_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (combo_Box_yasalinan_il.SelectedItem.ToString()==iller[0].ToString())
            {
                kat_sayi = 0.15;
            }
            if (combo_Box_yasalinan_il.SelectedItem.ToString() == iller[1].ToString() || 
                combo_Box_yasalinan_il.SelectedItem.ToString() == iller[2].ToString() )
            {
                kat_sayi = 0.10;
            }
            if (combo_Box_yasalinan_il.SelectedItem.ToString() == iller[3].ToString() || 
                combo_Box_yasalinan_il.SelectedItem.ToString() == iller[4].ToString() )
            {
                kat_sayi = 0.05;
            }
            if (combo_Box_yasalinan_il.SelectedItem.ToString() == iller[5].ToString() ||
                combo_Box_yasalinan_il.SelectedItem.ToString() == iller[6].ToString() ||
                combo_Box_yasalinan_il.SelectedItem.ToString() == iller[7].ToString() ||
                combo_Box_yasalinan_il.SelectedItem.ToString() == iller[8].ToString() ||
                combo_Box_yasalinan_il.SelectedItem.ToString() == iller[9].ToString() ||
                combo_Box_yasalinan_il.SelectedItem.ToString() == iller[10].ToString()) 
            {
                kat_sayi = 0.03;
            }
            if (combo_Box_yasalinan_il.SelectedItem.ToString() == iller[11].ToString())
            {
                kat_sayi=0.0;
            }
            slry.yasanilan_İl_Katsayisi = kat_sayi;
        }
        private void combo_Box_Akademik_Derece_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (combo_Box_Akademik_Derece.SelectedItem.ToString() == akademik_derece[0].ToString())
            {
                kat_sayi = 0.10;
            }
            if (combo_Box_Akademik_Derece.SelectedItem.ToString() == akademik_derece[1].ToString())
            {
                kat_sayi = 0.30;
            }
            if (combo_Box_Akademik_Derece.SelectedItem.ToString() == akademik_derece[2].ToString())
            {
                kat_sayi = 0.35;
            }
            if (combo_Box_Akademik_Derece.SelectedItem.ToString() == akademik_derece[3].ToString())
            {
                kat_sayi = 0.05;
            }
            if (combo_Box_Akademik_Derece.SelectedItem.ToString() == akademik_derece[4].ToString())
            {
                kat_sayi = 0.15;
            }
            if (combo_Box_Akademik_Derece.SelectedItem.ToString() == akademik_derece[5].ToString())
            {
                kat_sayi = 0.0;
            }
            slry.ust_Ogrenim_Katsayisi = kat_sayi;
        }
        private void combo_Box_Yöneticilik_Görevi_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (combo_Box_Yöneticilik_Görevi.SelectedItem.ToString() == Yöneticilik_Görevi[0].ToString())
            {
                kat_sayi = 0.50;
            }
            if (combo_Box_Yöneticilik_Görevi.SelectedItem.ToString() == Yöneticilik_Görevi[1].ToString())
            {
                kat_sayi = 0.75;
            }
            if (combo_Box_Yöneticilik_Görevi.SelectedItem.ToString() == Yöneticilik_Görevi[2].ToString())
            {
                kat_sayi = 0.85;
            }
            if (combo_Box_Yöneticilik_Görevi.SelectedItem.ToString() == Yöneticilik_Görevi[3].ToString())
            {
                kat_sayi = 1.0;
            }
            if (combo_Box_Yöneticilik_Görevi.SelectedItem.ToString() == Yöneticilik_Görevi[4].ToString())
            {
                kat_sayi = 0.40;
            }
            if (combo_Box_Yöneticilik_Görevi.SelectedItem.ToString() == Yöneticilik_Görevi[5].ToString())
            {
                kat_sayi = 0.60;
            }
            if (combo_Box_Yöneticilik_Görevi.SelectedItem.ToString() == Yöneticilik_Görevi[6].ToString())
            {
                kat_sayi = 0.0;
            }
            slry.yoneticilik_Gorevi_Katsayisi = kat_sayi;
        }
        private void checkBox_belgelendirilmiş_ing_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_belgelendirilmiş_ing.Checked == true)
            {
                kat_sayi = 0.20;
            }
            else
            {
                kat_sayi = 0.0;
            }
            total_dil += kat_sayi;
            slry.yabanci_Dil_Katsayisi = total_dil;
        }
        private void checkBox_ing_okul_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_ing_okul.Checked == true)
            {
                kat_sayi = 0.20;
            }
            else
            {
                kat_sayi = 0.0;
            }
            total_dil += kat_sayi;
            slry.yabanci_Dil_Katsayisi = total_dil;
        }
        private void checkBox_yabancı_dil_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_yabancı_dil.Checked==true)
            {
                btn_yabancı_dil_ekle.Show();
                textBox_yabancı_dil_ekle.Show();
                list_box_ekstra_diller.Show();
            }
            else
            {
                kat_sayi = 0.0;
                btn_yabancı_dil_ekle.Hide();
                textBox_yabancı_dil_ekle.Hide();
                list_box_ekstra_diller.Hide();
            }
            //Ekle butonuna basıldığında katsayı hesabı yapıyor.
        }
        private void btn_yabancı_dil_ekle_Click(object sender, EventArgs e)
        {
            bool kayıtlı;
            kayıtlı = list_box_ekstra_diller.Items.Contains(textBox_yabancı_dil_ekle.Text);

            if (kayıtlı == false && textBox_yabancı_dil_ekle.Text != "") 
            {
                list_box_ekstra_diller.Items.Add(textBox_yabancı_dil_ekle.Text);
                textBox_yabancı_dil_ekle.Text = "";
                kat_sayi = 0.05;
            }
            else if (kayıtlı==true)
            {
                MessageBox.Show("Dil kayıtlarda bulunuyor.");
            }
            total_dil += kat_sayi;
            slry.yabanci_Dil_Katsayisi = total_dil;
        }
        private void checkBox_çalışmıyor_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_çalışmıyor.Checked==true)
            {
                kat_sayi = 0.20;
            }
            else
            {
                kat_sayi = 0.0;
            }
            total_aile += kat_sayi;
            slry.aile_Durumu_Katsayisi = total_aile;
        }
        private void checkBox_0_6yaş_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_0_6yaş.Checked==true)
            {
                kat_sayi = 0.20;
            }
            else
            {
                kat_sayi = 0.0;
            }
            total_aile += kat_sayi;
            slry.aile_Durumu_Katsayisi = total_aile;
        }
        private void checkBox_7_18yas_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_7_18yas.Checked==true)
            {
                kat_sayi = 0.30;
            }
            else
            {
                kat_sayi = 0.0;
            }
            total_aile += kat_sayi;
            slry.aile_Durumu_Katsayisi = total_aile;
        }
        private void checkBox_18yas_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_18yas.Checked==true)
            {
                kat_sayi = 0.40;
            }
             else
            {
                kat_sayi = 0.0;
            }

            total_aile += kat_sayi;
            slry.aile_Durumu_Katsayisi = total_aile;
        }
        private void checkBox_2cocuk_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_2cocuk.Checked==true)
            {
                if (checkBox_çalışmıyor.Checked==true)
                {
                    total_aile =total_aile- 0.20;   // evli-çalışmayanı çıkar
                }
                total_aile = total_aile * 2;
            }
            slry.aile_Durumu_Katsayisi = total_aile;
        }

        private void btn_calculater_Click(object sender, EventArgs e)
        {
            Salary_Builder sb;

            if (LoginedUser.getInstance().User.UserType == 3)
            {
                sb = new Part_time_salary(slry);
                sb.maas_hesapla();
                result_salary = sb.Salary.total_Salary;
                sb.Salary.total_Salary = 0.0;
            }
            else
            {
                sb = new User_salary_builder(slry);
                sb.maas_hesapla();
                result_salary = sb.Salary.total_Salary;
                sb.Salary.total_Salary = 0.0;
            }
            this.Hide();
            PIWindow pıw = new PIWindow();
            pıw.Show();
        }
        private void SalaryCalculator_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
        private void btn_mainwindow_Click(object sender, EventArgs e)
        {
            this.Hide();
            MainWindow mainWindow = new MainWindow();
            mainWindow.Show();
        }
    }
}
