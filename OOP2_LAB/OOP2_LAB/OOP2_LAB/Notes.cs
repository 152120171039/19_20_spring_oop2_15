﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP2_LAB
{
    public class Notes
    {
        private String note;
        private String whoEntered;
        public Notes(String note, String whoEntered)
        {
            this.note = note;
            this.whoEntered = whoEntered;
        }
        public string WhoEntered { get => whoEntered; set => whoEntered = value; }
        public string Note { get => note; set => note = value; }
        public bool IsValid(string note)
        {
            return  this.note.Equals(note);
        }
        public string ToString_save()
        {
            return Note + "," + WhoEntered;
        }
        public string add_note()
        {
            return Note;
        }
    }
}
