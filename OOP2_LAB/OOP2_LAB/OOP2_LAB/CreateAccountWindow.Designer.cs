﻿namespace OOP2_LAB
{
    partial class CreateAccountWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbCreateUsername = new System.Windows.Forms.TextBox();
            this.tbCreatePassword = new System.Windows.Forms.TextBox();
            this.lbCreateUsername = new System.Windows.Forms.Label();
            this.lbCreatePassword = new System.Windows.Forms.Label();
            this.btnCreate = new System.Windows.Forms.Button();
            this.lbCheckUsername = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // tbCreateUsername
            // 
            this.tbCreateUsername.Location = new System.Drawing.Point(128, 55);
            this.tbCreateUsername.Name = "tbCreateUsername";
            this.tbCreateUsername.Size = new System.Drawing.Size(100, 20);
            this.tbCreateUsername.TabIndex = 0;
            this.tbCreateUsername.TextChanged += new System.EventHandler(this.tbCreateUsername_TextChanged);
            // 
            // tbCreatePassword
            // 
            this.tbCreatePassword.Location = new System.Drawing.Point(128, 111);
            this.tbCreatePassword.Name = "tbCreatePassword";
            this.tbCreatePassword.Size = new System.Drawing.Size(100, 20);
            this.tbCreatePassword.TabIndex = 1;
            this.tbCreatePassword.UseSystemPasswordChar = true;
            // 
            // lbCreateUsername
            // 
            this.lbCreateUsername.AutoSize = true;
            this.lbCreateUsername.Location = new System.Drawing.Point(67, 58);
            this.lbCreateUsername.Name = "lbCreateUsername";
            this.lbCreateUsername.Size = new System.Drawing.Size(55, 13);
            this.lbCreateUsername.TabIndex = 2;
            this.lbCreateUsername.Text = "Username";
            // 
            // lbCreatePassword
            // 
            this.lbCreatePassword.AutoSize = true;
            this.lbCreatePassword.Location = new System.Drawing.Point(69, 114);
            this.lbCreatePassword.Name = "lbCreatePassword";
            this.lbCreatePassword.Size = new System.Drawing.Size(53, 13);
            this.lbCreatePassword.TabIndex = 3;
            this.lbCreatePassword.Text = "Password";
            // 
            // btnCreate
            // 
            this.btnCreate.Cursor = System.Windows.Forms.Cursors.Default;
            this.btnCreate.Location = new System.Drawing.Point(112, 148);
            this.btnCreate.Name = "btnCreate";
            this.btnCreate.Size = new System.Drawing.Size(75, 23);
            this.btnCreate.TabIndex = 4;
            this.btnCreate.Text = "Create";
            this.btnCreate.UseVisualStyleBackColor = true;
            this.btnCreate.Click += new System.EventHandler(this.btnCreate_Click);
            // 
            // lbCheckUsername
            // 
            this.lbCheckUsername.AutoSize = true;
            this.lbCheckUsername.Location = new System.Drawing.Point(87, 88);
            this.lbCheckUsername.Name = "lbCheckUsername";
            this.lbCheckUsername.Size = new System.Drawing.Size(0, 13);
            this.lbCheckUsername.TabIndex = 5;
            // 
            // CreateAccountWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(308, 215);
            this.Controls.Add(this.lbCheckUsername);
            this.Controls.Add(this.btnCreate);
            this.Controls.Add(this.lbCreatePassword);
            this.Controls.Add(this.lbCreateUsername);
            this.Controls.Add(this.tbCreatePassword);
            this.Controls.Add(this.tbCreateUsername);
            this.Name = "CreateAccountWindow";
            this.Text = "CreateAccountWindow";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CreateAccountWindow_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbCreateUsername;
        private System.Windows.Forms.TextBox tbCreatePassword;
        private System.Windows.Forms.Label lbCreateUsername;
        private System.Windows.Forms.Label lbCreatePassword;
        private System.Windows.Forms.Button btnCreate;
        private System.Windows.Forms.Label lbCheckUsername;
    }
}