﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OOP2_LAB
{
    public partial class UMWindow : Form
    {
        public int selectedUsersTypeInlboxUserType;
        public UMWindow()
        {
            InitializeComponent();
            for (int i = 0; i < LoginWindow.userList.Count; i++)
            {
                lboxUserList.Items.Add(LoginWindow.userList[i].Username);
            }
            string[] userTypes = { "Admin", "User", "Part-Time User" };
            lboxUserType.Items.AddRange(userTypes);
        }
        
        private void UMWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void lboxUserList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (LoginWindow.userList[lboxUserList.SelectedIndex].UserType == 0 && LoginedUser.getInstance().User.UserType != 0)
            {
                MessageBox.Show("You can't see this user's information!", "Oops");
                lbWarningSelect.Visible = true;
                lbSelectedUser.Visible = false;
                lbUsername.Visible = false;
                lbUserType.Visible = false;
                txtUsername.Visible = false;
                lboxUserType.Visible = false;
                btnSave.Visible = false;
                return;
            }
            lbWarningSelect.Visible = false;
            lbSelectedUser.Visible = true;
            lbUsername.Visible = true;
            lbUserType.Visible = true;
            txtUsername.Visible = true;
            lboxUserType.Visible = true;
            btnSave.Visible = true;
            txtUsername.Text = LoginWindow.userList[lboxUserList.SelectedIndex].Username;
            if (LoginWindow.userList[lboxUserList.SelectedIndex].Username == LoginedUser.getInstance().User.Username)
            {
                lboxUserType.Enabled = false;
                MessageBox.Show("You can't change your user type.","Oops");
            }
            else
            {
                lboxUserType.Enabled = true;
            }
            if (LoginWindow.userList[lboxUserList.SelectedIndex].UserType == 0 || LoginWindow.userList[lboxUserList.SelectedIndex].UserType == 1)
            {
                selectedUsersTypeInlboxUserType = 0;
                lboxUserType.SetSelected(0, true);
            }
            else if (LoginWindow.userList[lboxUserList.SelectedIndex].UserType == 2)
            {
                selectedUsersTypeInlboxUserType = 1;
                lboxUserType.SetSelected(1, true);
            }
            else if (LoginWindow.userList[lboxUserList.SelectedIndex].UserType == 3)
            {
                selectedUsersTypeInlboxUserType = 2;
                lboxUserType.SetSelected(2, true);
            }
            
        }

        private void lboxUserType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lboxUserType.SelectedIndex != selectedUsersTypeInlboxUserType)
            {
                btnSave.Enabled = true;
            }
            else
            {
                btnSave.Enabled = false;
            }

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (lboxUserType.SelectedIndex == 0)
            {
                selectedUsersTypeInlboxUserType = 0;
                LoginWindow.userList[lboxUserList.SelectedIndex].UserType = 1;
            }
            else if (lboxUserType.SelectedIndex == 1)
            {
                selectedUsersTypeInlboxUserType = 1;
                LoginWindow.userList[lboxUserList.SelectedIndex].UserType = 2;
            }
            else if (lboxUserType.SelectedIndex == 2)
            {
                selectedUsersTypeInlboxUserType = 2;
                LoginWindow.userList[lboxUserList.SelectedIndex].UserType = 3;
            }
            btnSave.Enabled = false;
            Util.SaveCsv(LoginWindow.userList, LoginWindow.userFilePath);
            MessageBox.Show("You have saved changes succesfully.", "User Management");
        }

        private void btnMainWindow_Click(object sender, EventArgs e)
        {
            this.Hide();
            MainWindow mainWindow = new MainWindow();
            mainWindow.Show();
        }
    }
}
