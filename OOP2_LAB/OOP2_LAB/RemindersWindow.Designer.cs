﻿namespace OOP2_LAB
{
    partial class RemindersWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btn_mainwindow = new System.Windows.Forms.Button();
            this.lbl_meet = new System.Windows.Forms.Label();
            this.lbl_task = new System.Windows.Forms.Label();
            this.btn_meet_list = new System.Windows.Forms.Button();
            this.btn_meet_add = new System.Windows.Forms.Button();
            this.btn_task_add = new System.Windows.Forms.Button();
            this.btn_task_list = new System.Windows.Forms.Button();
            this.lbl_meet_time = new System.Windows.Forms.Label();
            this.lbl_meet_date = new System.Windows.Forms.Label();
            this.tb_meet_summary = new System.Windows.Forms.TextBox();
            this.lbl_meet_summary = new System.Windows.Forms.Label();
            this.tb_meet_description = new System.Windows.Forms.TextBox();
            this.lbl_meet_description = new System.Windows.Forms.Label();
            this.tb_task_description = new System.Windows.Forms.TextBox();
            this.lbl_task_description = new System.Windows.Forms.Label();
            this.tb_task_summary = new System.Windows.Forms.TextBox();
            this.lbl_task_summary = new System.Windows.Forms.Label();
            this.lbl_task_date = new System.Windows.Forms.Label();
            this.lbl_task_time = new System.Windows.Forms.Label();
            this.maskedTB_meet_time = new System.Windows.Forms.MaskedTextBox();
            this.maskedTB_task_time = new System.Windows.Forms.MaskedTextBox();
            this.timer_reminder = new System.Windows.Forms.Timer(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.timer_RealTime = new System.Windows.Forms.Timer(this.components);
            this.tb_meet_date = new System.Windows.Forms.TextBox();
            this.tb_task_date = new System.Windows.Forms.TextBox();
            this.lbl_meet_example = new System.Windows.Forms.Label();
            this.lbl_task_example = new System.Windows.Forms.Label();
            this.timer_reminder_task = new System.Windows.Forms.Timer(this.components);
            this.dataGridView_ListMeet = new System.Windows.Forms.DataGridView();
            this.dataGridView_ListTask = new System.Windows.Forms.DataGridView();
            this.btn_meet_delete = new System.Windows.Forms.Button();
            this.btn_task_delete = new System.Windows.Forms.Button();
            this.btn_meet_update = new System.Windows.Forms.Button();
            this.btn_task_update = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_ListMeet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_ListTask)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_mainwindow
            // 
            this.btn_mainwindow.Location = new System.Drawing.Point(1157, 28);
            this.btn_mainwindow.Name = "btn_mainwindow";
            this.btn_mainwindow.Size = new System.Drawing.Size(110, 57);
            this.btn_mainwindow.TabIndex = 0;
            this.btn_mainwindow.Text = "Back to Main Window";
            this.btn_mainwindow.UseVisualStyleBackColor = true;
            this.btn_mainwindow.Click += new System.EventHandler(this.btn_mainwindow_Click);
            // 
            // lbl_meet
            // 
            this.lbl_meet.AutoSize = true;
            this.lbl_meet.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl_meet.ForeColor = System.Drawing.Color.Maroon;
            this.lbl_meet.Location = new System.Drawing.Point(137, 28);
            this.lbl_meet.Name = "lbl_meet";
            this.lbl_meet.Size = new System.Drawing.Size(149, 32);
            this.lbl_meet.TabIndex = 1;
            this.lbl_meet.Text = "MEETING";
            // 
            // lbl_task
            // 
            this.lbl_task.AutoSize = true;
            this.lbl_task.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl_task.ForeColor = System.Drawing.Color.Maroon;
            this.lbl_task.Location = new System.Drawing.Point(828, 28);
            this.lbl_task.Name = "lbl_task";
            this.lbl_task.Size = new System.Drawing.Size(93, 32);
            this.lbl_task.TabIndex = 2;
            this.lbl_task.Text = "TASK";
            // 
            // btn_meet_list
            // 
            this.btn_meet_list.Location = new System.Drawing.Point(153, 89);
            this.btn_meet_list.Name = "btn_meet_list";
            this.btn_meet_list.Size = new System.Drawing.Size(72, 35);
            this.btn_meet_list.TabIndex = 3;
            this.btn_meet_list.Text = "list";
            this.btn_meet_list.UseVisualStyleBackColor = true;
            this.btn_meet_list.Click += new System.EventHandler(this.btn_meet_list_Click);
            // 
            // btn_meet_add
            // 
            this.btn_meet_add.Location = new System.Drawing.Point(77, 89);
            this.btn_meet_add.Name = "btn_meet_add";
            this.btn_meet_add.Size = new System.Drawing.Size(72, 35);
            this.btn_meet_add.TabIndex = 4;
            this.btn_meet_add.Text = "add";
            this.btn_meet_add.UseVisualStyleBackColor = true;
            this.btn_meet_add.Click += new System.EventHandler(this.btn_meet_add_Click);
            // 
            // btn_task_add
            // 
            this.btn_task_add.Location = new System.Drawing.Point(724, 89);
            this.btn_task_add.Name = "btn_task_add";
            this.btn_task_add.Size = new System.Drawing.Size(72, 35);
            this.btn_task_add.TabIndex = 8;
            this.btn_task_add.Text = "add";
            this.btn_task_add.UseVisualStyleBackColor = true;
            this.btn_task_add.Click += new System.EventHandler(this.btn_task_add_Click);
            // 
            // btn_task_list
            // 
            this.btn_task_list.Location = new System.Drawing.Point(802, 89);
            this.btn_task_list.Name = "btn_task_list";
            this.btn_task_list.Size = new System.Drawing.Size(72, 35);
            this.btn_task_list.TabIndex = 7;
            this.btn_task_list.Text = "list";
            this.btn_task_list.UseVisualStyleBackColor = true;
            this.btn_task_list.Click += new System.EventHandler(this.btn_task_list_Click);
            // 
            // lbl_meet_time
            // 
            this.lbl_meet_time.AutoSize = true;
            this.lbl_meet_time.Location = new System.Drawing.Point(74, 155);
            this.lbl_meet_time.Name = "lbl_meet_time";
            this.lbl_meet_time.Size = new System.Drawing.Size(43, 17);
            this.lbl_meet_time.TabIndex = 11;
            this.lbl_meet_time.Text = "Time ";
            // 
            // lbl_meet_date
            // 
            this.lbl_meet_date.AutoSize = true;
            this.lbl_meet_date.Location = new System.Drawing.Point(74, 229);
            this.lbl_meet_date.Name = "lbl_meet_date";
            this.lbl_meet_date.Size = new System.Drawing.Size(38, 17);
            this.lbl_meet_date.TabIndex = 13;
            this.lbl_meet_date.Text = "Date";
            // 
            // tb_meet_summary
            // 
            this.tb_meet_summary.Location = new System.Drawing.Point(143, 276);
            this.tb_meet_summary.Multiline = true;
            this.tb_meet_summary.Name = "tb_meet_summary";
            this.tb_meet_summary.Size = new System.Drawing.Size(240, 48);
            this.tb_meet_summary.TabIndex = 16;
            // 
            // lbl_meet_summary
            // 
            this.lbl_meet_summary.AutoSize = true;
            this.lbl_meet_summary.Location = new System.Drawing.Point(50, 291);
            this.lbl_meet_summary.Name = "lbl_meet_summary";
            this.lbl_meet_summary.Size = new System.Drawing.Size(67, 17);
            this.lbl_meet_summary.TabIndex = 15;
            this.lbl_meet_summary.Text = "Summary";
            // 
            // tb_meet_description
            // 
            this.tb_meet_description.Location = new System.Drawing.Point(143, 352);
            this.tb_meet_description.Multiline = true;
            this.tb_meet_description.Name = "tb_meet_description";
            this.tb_meet_description.Size = new System.Drawing.Size(240, 137);
            this.tb_meet_description.TabIndex = 18;
            // 
            // lbl_meet_description
            // 
            this.lbl_meet_description.AutoSize = true;
            this.lbl_meet_description.Location = new System.Drawing.Point(45, 416);
            this.lbl_meet_description.Name = "lbl_meet_description";
            this.lbl_meet_description.Size = new System.Drawing.Size(79, 17);
            this.lbl_meet_description.TabIndex = 17;
            this.lbl_meet_description.Text = "Description";
            // 
            // tb_task_description
            // 
            this.tb_task_description.Location = new System.Drawing.Point(790, 352);
            this.tb_task_description.Multiline = true;
            this.tb_task_description.Name = "tb_task_description";
            this.tb_task_description.Size = new System.Drawing.Size(240, 137);
            this.tb_task_description.TabIndex = 26;
            // 
            // lbl_task_description
            // 
            this.lbl_task_description.AutoSize = true;
            this.lbl_task_description.Location = new System.Drawing.Point(692, 416);
            this.lbl_task_description.Name = "lbl_task_description";
            this.lbl_task_description.Size = new System.Drawing.Size(79, 17);
            this.lbl_task_description.TabIndex = 25;
            this.lbl_task_description.Text = "Description";
            // 
            // tb_task_summary
            // 
            this.tb_task_summary.Location = new System.Drawing.Point(790, 276);
            this.tb_task_summary.Multiline = true;
            this.tb_task_summary.Name = "tb_task_summary";
            this.tb_task_summary.Size = new System.Drawing.Size(240, 48);
            this.tb_task_summary.TabIndex = 24;
            // 
            // lbl_task_summary
            // 
            this.lbl_task_summary.AutoSize = true;
            this.lbl_task_summary.Location = new System.Drawing.Point(697, 291);
            this.lbl_task_summary.Name = "lbl_task_summary";
            this.lbl_task_summary.Size = new System.Drawing.Size(67, 17);
            this.lbl_task_summary.TabIndex = 23;
            this.lbl_task_summary.Text = "Summary";
            // 
            // lbl_task_date
            // 
            this.lbl_task_date.AutoSize = true;
            this.lbl_task_date.Location = new System.Drawing.Point(721, 229);
            this.lbl_task_date.Name = "lbl_task_date";
            this.lbl_task_date.Size = new System.Drawing.Size(38, 17);
            this.lbl_task_date.TabIndex = 21;
            this.lbl_task_date.Text = "Date";
            // 
            // lbl_task_time
            // 
            this.lbl_task_time.AutoSize = true;
            this.lbl_task_time.Location = new System.Drawing.Point(721, 155);
            this.lbl_task_time.Name = "lbl_task_time";
            this.lbl_task_time.Size = new System.Drawing.Size(43, 17);
            this.lbl_task_time.TabIndex = 19;
            this.lbl_task_time.Text = "Time ";
            // 
            // maskedTB_meet_time
            // 
            this.maskedTB_meet_time.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.maskedTB_meet_time.Location = new System.Drawing.Point(143, 155);
            this.maskedTB_meet_time.Mask = "00:00:00";
            this.maskedTB_meet_time.Name = "maskedTB_meet_time";
            this.maskedTB_meet_time.Size = new System.Drawing.Size(130, 27);
            this.maskedTB_meet_time.TabIndex = 27;
            // 
            // maskedTB_task_time
            // 
            this.maskedTB_task_time.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.maskedTB_task_time.Location = new System.Drawing.Point(790, 155);
            this.maskedTB_task_time.Mask = "00:00:00";
            this.maskedTB_task_time.Name = "maskedTB_task_time";
            this.maskedTB_task_time.Size = new System.Drawing.Size(130, 27);
            this.maskedTB_task_time.TabIndex = 28;
            // 
            // timer_reminder
            // 
            this.timer_reminder.Interval = 1000;
            this.timer_reminder.Tick += new System.EventHandler(this.timer_reminder_Tick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(502, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 17);
            this.label1.TabIndex = 33;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(461, 107);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 17);
            this.label2.TabIndex = 34;
            // 
            // timer_RealTime
            // 
            this.timer_RealTime.Interval = 1000;
            this.timer_RealTime.Tick += new System.EventHandler(this.timer_RealTime_Tick);
            // 
            // tb_meet_date
            // 
            this.tb_meet_date.Location = new System.Drawing.Point(143, 231);
            this.tb_meet_date.Name = "tb_meet_date";
            this.tb_meet_date.Size = new System.Drawing.Size(223, 22);
            this.tb_meet_date.TabIndex = 36;
            // 
            // tb_task_date
            // 
            this.tb_task_date.Location = new System.Drawing.Point(790, 231);
            this.tb_task_date.Name = "tb_task_date";
            this.tb_task_date.Size = new System.Drawing.Size(223, 22);
            this.tb_task_date.TabIndex = 37;
            // 
            // lbl_meet_example
            // 
            this.lbl_meet_example.AutoSize = true;
            this.lbl_meet_example.Location = new System.Drawing.Point(140, 202);
            this.lbl_meet_example.Name = "lbl_meet_example";
            this.lbl_meet_example.Size = new System.Drawing.Size(195, 17);
            this.lbl_meet_example.TabIndex = 38;
            this.lbl_meet_example.Text = "EXP: 6 Mayıs 2020 Çarşamba";
            // 
            // lbl_task_example
            // 
            this.lbl_task_example.AutoSize = true;
            this.lbl_task_example.Location = new System.Drawing.Point(787, 202);
            this.lbl_task_example.Name = "lbl_task_example";
            this.lbl_task_example.Size = new System.Drawing.Size(195, 17);
            this.lbl_task_example.TabIndex = 39;
            this.lbl_task_example.Text = "EXP: 6 Mayıs 2020 Çarşamba";
            // 
            // timer_reminder_task
            // 
            this.timer_reminder_task.Tick += new System.EventHandler(this.timer_reminder_task_Tick);
            // 
            // dataGridView_ListMeet
            // 
            this.dataGridView_ListMeet.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridView_ListMeet.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView_ListMeet.DefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridView_ListMeet.Location = new System.Drawing.Point(12, 549);
            this.dataGridView_ListMeet.Name = "dataGridView_ListMeet";
            this.dataGridView_ListMeet.RowTemplate.Height = 24;
            this.dataGridView_ListMeet.Size = new System.Drawing.Size(596, 239);
            this.dataGridView_ListMeet.TabIndex = 41;
            this.dataGridView_ListMeet.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_ListMeet_CellClick);
            // 
            // dataGridView_ListTask
            // 
            this.dataGridView_ListTask.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridView_ListTask.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView_ListTask.DefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridView_ListTask.Location = new System.Drawing.Point(642, 549);
            this.dataGridView_ListTask.Name = "dataGridView_ListTask";
            this.dataGridView_ListTask.RowTemplate.Height = 24;
            this.dataGridView_ListTask.Size = new System.Drawing.Size(596, 239);
            this.dataGridView_ListTask.TabIndex = 41;
            this.dataGridView_ListTask.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_ListTask_CellClick);
            // 
            // btn_meet_delete
            // 
            this.btn_meet_delete.Location = new System.Drawing.Point(233, 89);
            this.btn_meet_delete.Name = "btn_meet_delete";
            this.btn_meet_delete.Size = new System.Drawing.Size(72, 35);
            this.btn_meet_delete.TabIndex = 42;
            this.btn_meet_delete.Text = "delete";
            this.btn_meet_delete.UseVisualStyleBackColor = true;
            this.btn_meet_delete.Click += new System.EventHandler(this.btn_meet_delete_Click);
            // 
            // btn_task_delete
            // 
            this.btn_task_delete.Location = new System.Drawing.Point(880, 89);
            this.btn_task_delete.Name = "btn_task_delete";
            this.btn_task_delete.Size = new System.Drawing.Size(72, 35);
            this.btn_task_delete.TabIndex = 43;
            this.btn_task_delete.Text = "delete";
            this.btn_task_delete.UseVisualStyleBackColor = true;
            this.btn_task_delete.Click += new System.EventHandler(this.btn_task_delete_Click);
            // 
            // btn_meet_update
            // 
            this.btn_meet_update.Location = new System.Drawing.Point(311, 89);
            this.btn_meet_update.Name = "btn_meet_update";
            this.btn_meet_update.Size = new System.Drawing.Size(72, 35);
            this.btn_meet_update.TabIndex = 44;
            this.btn_meet_update.Text = "update";
            this.btn_meet_update.UseVisualStyleBackColor = true;
            this.btn_meet_update.Click += new System.EventHandler(this.btn_meet_update_Click);
            // 
            // btn_task_update
            // 
            this.btn_task_update.Location = new System.Drawing.Point(958, 89);
            this.btn_task_update.Name = "btn_task_update";
            this.btn_task_update.Size = new System.Drawing.Size(72, 35);
            this.btn_task_update.TabIndex = 45;
            this.btn_task_update.Text = "update";
            this.btn_task_update.UseVisualStyleBackColor = true;
            this.btn_task_update.Click += new System.EventHandler(this.btn_task_update_Click);
            // 
            // RemindersWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1284, 1055);
            this.Controls.Add(this.btn_task_update);
            this.Controls.Add(this.btn_meet_update);
            this.Controls.Add(this.btn_task_delete);
            this.Controls.Add(this.btn_meet_delete);
            this.Controls.Add(this.dataGridView_ListTask);
            this.Controls.Add(this.dataGridView_ListMeet);
            this.Controls.Add(this.lbl_task_example);
            this.Controls.Add(this.lbl_meet_example);
            this.Controls.Add(this.tb_task_date);
            this.Controls.Add(this.tb_meet_date);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.maskedTB_task_time);
            this.Controls.Add(this.maskedTB_meet_time);
            this.Controls.Add(this.tb_task_description);
            this.Controls.Add(this.lbl_task_description);
            this.Controls.Add(this.tb_task_summary);
            this.Controls.Add(this.lbl_task_summary);
            this.Controls.Add(this.lbl_task_date);
            this.Controls.Add(this.lbl_task_time);
            this.Controls.Add(this.tb_meet_description);
            this.Controls.Add(this.lbl_meet_description);
            this.Controls.Add(this.tb_meet_summary);
            this.Controls.Add(this.lbl_meet_summary);
            this.Controls.Add(this.lbl_meet_date);
            this.Controls.Add(this.lbl_meet_time);
            this.Controls.Add(this.btn_task_add);
            this.Controls.Add(this.btn_task_list);
            this.Controls.Add(this.btn_meet_add);
            this.Controls.Add(this.btn_meet_list);
            this.Controls.Add(this.lbl_task);
            this.Controls.Add(this.lbl_meet);
            this.Controls.Add(this.btn_mainwindow);
            this.Name = "RemindersWindow";
            this.Text = "Reminders";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Reminders_FormClosed);
            this.Load += new System.EventHandler(this.Reminders_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_ListMeet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_ListTask)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_mainwindow;
        private System.Windows.Forms.Label lbl_meet;
        private System.Windows.Forms.Label lbl_task;
        private System.Windows.Forms.Button btn_meet_list;
        private System.Windows.Forms.Button btn_meet_add;
        private System.Windows.Forms.Button btn_task_add;
        private System.Windows.Forms.Button btn_task_list;
        private System.Windows.Forms.Label lbl_meet_time;
        private System.Windows.Forms.Label lbl_meet_date;
        private System.Windows.Forms.TextBox tb_meet_summary;
        private System.Windows.Forms.Label lbl_meet_summary;
        private System.Windows.Forms.TextBox tb_meet_description;
        private System.Windows.Forms.Label lbl_meet_description;
        private System.Windows.Forms.TextBox tb_task_description;
        private System.Windows.Forms.Label lbl_task_description;
        private System.Windows.Forms.TextBox tb_task_summary;
        private System.Windows.Forms.Label lbl_task_summary;
        private System.Windows.Forms.Label lbl_task_date;
        private System.Windows.Forms.Label lbl_task_time;
        private System.Windows.Forms.MaskedTextBox maskedTB_meet_time;
        private System.Windows.Forms.MaskedTextBox maskedTB_task_time;
        private System.Windows.Forms.Timer timer_reminder;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Timer timer_RealTime;
        private System.Windows.Forms.TextBox tb_meet_date;
        private System.Windows.Forms.TextBox tb_task_date;
        private System.Windows.Forms.Label lbl_meet_example;
        private System.Windows.Forms.Label lbl_task_example;
        private System.Windows.Forms.Timer timer_reminder_task;
        private System.Windows.Forms.DataGridView dataGridView_ListMeet;
        private System.Windows.Forms.DataGridView dataGridView_ListTask;
        private System.Windows.Forms.Button btn_meet_delete;
        private System.Windows.Forms.Button btn_task_delete;
        private System.Windows.Forms.Button btn_meet_update;
        private System.Windows.Forms.Button btn_task_update;
    }
}