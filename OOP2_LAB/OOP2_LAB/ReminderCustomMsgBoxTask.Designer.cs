﻿namespace OOP2_LAB
{
    partial class ReminderCustomMsgBoxTask
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel_task = new System.Windows.Forms.Panel();
            this.lbl_task_msgbx = new System.Windows.Forms.Label();
            this.button_task_OK = new System.Windows.Forms.Button();
            this.timer_MsgBox_task = new System.Windows.Forms.Timer(this.components);
            this.panel_task.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel_task
            // 
            this.panel_task.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.panel_task.Controls.Add(this.lbl_task_msgbx);
            this.panel_task.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel_task.Location = new System.Drawing.Point(0, 0);
            this.panel_task.Name = "panel_task";
            this.panel_task.Size = new System.Drawing.Size(722, 160);
            this.panel_task.TabIndex = 0;
            // 
            // lbl_task_msgbx
            // 
            this.lbl_task_msgbx.AutoSize = true;
            this.lbl_task_msgbx.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl_task_msgbx.Location = new System.Drawing.Point(25, 45);
            this.lbl_task_msgbx.Name = "lbl_task_msgbx";
            this.lbl_task_msgbx.Size = new System.Drawing.Size(0, 25);
            this.lbl_task_msgbx.TabIndex = 0;
            // 
            // button_task_OK
            // 
            this.button_task_OK.Location = new System.Drawing.Point(274, 209);
            this.button_task_OK.Name = "button_task_OK";
            this.button_task_OK.Size = new System.Drawing.Size(128, 60);
            this.button_task_OK.TabIndex = 1;
            this.button_task_OK.Text = "OK";
            this.button_task_OK.UseVisualStyleBackColor = true;
            this.button_task_OK.Click += new System.EventHandler(this.button_task_OK_Click);
            // 
            // timer_MsgBox_task
            // 
            this.timer_MsgBox_task.Tick += new System.EventHandler(this.timer_MsgBox_task_Tick);
            // 
            // ReminderCustomMsgBoxTask
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(722, 313);
            this.Controls.Add(this.button_task_OK);
            this.Controls.Add(this.panel_task);
            this.Name = "ReminderCustomMsgBoxTask";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ReminderCustomMsgBoxTask";
            this.Load += new System.EventHandler(this.ReminderCustomMsgBoxTask_Load);
            this.panel_task.ResumeLayout(false);
            this.panel_task.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel_task;
        private System.Windows.Forms.Button button_task_OK;
        private System.Windows.Forms.Timer timer_MsgBox_task;
        private System.Windows.Forms.Label lbl_task_msgbx;
    }
}