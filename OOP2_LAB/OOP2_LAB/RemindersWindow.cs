﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OOP2_LAB
{
    public partial class RemindersWindow : Form
    {
        public static List<Reminders> RemindersList = new List<Reminders>();
        public const string RemindersFilePath = @"Reminders.csv";
        string  time, date, summary, description, reminder_type, whoEntered,real_time,real_date;
        public static int choose_meet = -1;
        public static int choose_task = -1;
        public static int selected_meet = -1; //update meet
        public static int selected_task = -1; //update task
        public static int index_meet = -1;
        public static int index_task = -1;

        public RemindersWindow()
        {
            InitializeComponent();
        }

        private void btn_mainwindow_Click(object sender, EventArgs e)
        {
            this.Hide();
            MainWindow mainWindow = new MainWindow();
            mainWindow.Show();
        }

        private void timer_RealTime_Tick(object sender, EventArgs e)
        {
            real_time = DateTime.Now.ToLongTimeString();
            real_date = DateTime.Now.ToLongDateString();
            label1.Text = real_time;
            label2.Text = real_date;
        }
        
        public void LoadMeetReminders(List<Reminders> reminders)
        {
            for (int i = 0; i < reminders.Count; i++)
            {
                Reminders tmp = new Reminders(time, date, summary, description, reminder_type, whoEntered);
                tmp = (Reminders)reminders[i];
                string[] row = { tmp.Time, tmp.Date, tmp.Summary, tmp.Description, tmp.Reminder_type, tmp.WhoEntered };

                if (row[0] == real_time.ToString() && row[1] == real_date.ToString() && row[4]=="M")
                {
                    timer_reminder.Stop();
                    ReminderCustomMsgBox.Show(row[2],"MEETING");
                }
                timer_reminder.Start();
            }
        }

        public void LoadTaskReminders(List<Reminders> reminders)
        {
            for (int i = 0; i < reminders.Count; i++)
            {
                Reminders tmp = new Reminders(time, date, summary, description, reminder_type, whoEntered);
                tmp = (Reminders)reminders[i];
                string[] row = { tmp.Time, tmp.Date, tmp.Summary, tmp.Description, tmp.Reminder_type, tmp.WhoEntered };

                if (row[0] == real_time.ToString() && row[1] == real_date.ToString() && row[4] == "T")
                {
                    timer_reminder_task.Stop();
                    ReminderCustomMsgBoxTask.Show(row[2], "TASK");
                }
                timer_reminder_task.Start();
            }
        }

        private void timer_reminder_Tick(object sender, EventArgs e)
        {
            LoadMeetReminders(LoginedUser.getInstance().User.userReminders);
        }

        private void timer_reminder_task_Tick(object sender, EventArgs e)
        {
            LoadTaskReminders(LoginedUser.getInstance().User.userReminders);
        }

        private void btn_meet_add_Click(object sender, EventArgs e)
        {
            string time = maskedTB_meet_time.Text;
            string date = tb_meet_date.Text;
            string summary = tb_meet_summary.Text;
            string description = tb_meet_description.Text;
            string reminder_type = "M";

            if (time != "" && date != "" && summary != "" && description != "")
            {
                Reminders Reminders = new Reminders(time, date, summary, description, reminder_type, LoginedUser.getInstance().User.Username);
                LoginedUser.getInstance().User.userReminders.Add(Reminders);
                for (int i = 0; i < LoginWindow.userList.Count; i++)
                {
                    if (LoginWindow.userList[i].Username == LoginedUser.getInstance().User.Username)
                    {
                        LoginWindow.userList[i] = LoginedUser.getInstance().User;
                    }
                }
                if (RemindersList.Count != 0)
                {
                    RemindersList.Clear();
                }
                foreach (User user in LoginWindow.userList)
                {
                    RemindersList.AddRange(user.userReminders);
                }
                Util.SaveCsv(RemindersList, RemindersFilePath);
            }
            else
            {
                MessageBox.Show("Error ! ");
            }
            Tb_Empty_Meet();
            timer_reminder.Start();
        }

        private void btn_meet_list_Click(object sender, EventArgs e)
        {
            ListMeetReminders(LoginedUser.getInstance().User.userReminders);
            dataGridView_ListMeet.Show();
            foreach(DataGridViewRow row in dataGridView_ListMeet.Rows)
            { row.ReadOnly = true; }
            Tb_Empty_Meet();
        }

        private void btn_task_list_Click(object sender, EventArgs e)
        {
            ListTaskReminders(LoginedUser.getInstance().User.userReminders);
            dataGridView_ListTask.Show();
            foreach (DataGridViewRow row in dataGridView_ListTask.Rows)
            { row.ReadOnly = true; }
            Tb_Empty_Task();
        }

        private void btn_meet_delete_Click(object sender, EventArgs e)
        {
            if (LoginedUser.getInstance().User.userReminders.Count != 0)
            {
                DialogResult result = MessageBox.Show("Do you want to delete the reminder ? ", "Deleting ...", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (result == DialogResult.Yes)
                {
                    for (int i = 0; i < LoginedUser.getInstance().User.userReminders.Count; i++)
                    {
                        Reminders tmp = new Reminders(time, date, summary, description, reminder_type, whoEntered);
                        tmp = (Reminders)LoginedUser.getInstance().User.userReminders[i];
                        string[] row = { tmp.Time, tmp.Date, tmp.Summary, tmp.Description, tmp.Reminder_type, tmp.WhoEntered };
                        choose_meet = dataGridView_ListMeet.CurrentCell.RowIndex;
                        string _time = dataGridView_ListMeet.Rows[choose_meet].Cells[0].Value.ToString();
                        string _date = dataGridView_ListMeet.Rows[choose_meet].Cells[1].Value.ToString();
                        string _sum = dataGridView_ListMeet.Rows[choose_meet].Cells[2].Value.ToString();
                        string _des = dataGridView_ListMeet.Rows[choose_meet].Cells[3].Value.ToString();
                        string _type = "M";
                        if (row[0] == _time && row[1] == _date && row[2] == _sum && row[3] == _des && row[4] == "M")
                        {
                            dataGridView_ListMeet.Rows.RemoveAt(choose_meet);
                            LoginedUser.getInstance().User.userReminders.RemoveAt(i);
                            ListMeetReminders(LoginedUser.getInstance().User.userReminders);
                            for (int k = 0; k < LoginWindow.userList.Count; k++)
                            {
                                if (LoginWindow.userList[k].Username == LoginedUser.getInstance().User.Username)
                                {
                                    LoginWindow.userList[k] = LoginedUser.getInstance().User;
                                }
                            }
                            if (RemindersList.Count != 0)
                            {
                                RemindersList.Clear();
                            }
                            foreach (User user in LoginWindow.userList)
                            {
                                RemindersList.AddRange(user.userReminders);
                            }
                            Util.SaveCsv(RemindersList, RemindersFilePath);
                            Tb_Empty_Meet();
                            choose_meet = -1;
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("Meeting is Empty!");
                btn_meet_delete.Enabled = false;
                btn_meet_update.Enabled = false;
            }
        }

        private void btn_task_delete_Click(object sender, EventArgs e)
        {
            if (LoginedUser.getInstance().User.userReminders.Count != 0)
            {
                DialogResult result = MessageBox.Show("Do you want to delete the reminder ? ", "Deleting ...", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (result == DialogResult.Yes)
                {
                    for (int i = 0; i < LoginedUser.getInstance().User.userReminders.Count; i++)
                    {
                        Reminders tmp = new Reminders(time, date, summary, description, reminder_type, whoEntered);
                        tmp = (Reminders)LoginedUser.getInstance().User.userReminders[i];
                        string[] row = { tmp.Time, tmp.Date, tmp.Summary, tmp.Description, tmp.Reminder_type, tmp.WhoEntered };
                        choose_task = dataGridView_ListTask.CurrentCell.RowIndex;
                        string _time = dataGridView_ListTask.Rows[choose_task].Cells[0].Value.ToString();
                        string _date = dataGridView_ListTask.Rows[choose_task].Cells[1].Value.ToString();
                        string _sum = dataGridView_ListTask.Rows[choose_task].Cells[2].Value.ToString();
                        string _des = dataGridView_ListTask.Rows[choose_task].Cells[3].Value.ToString();
                        if (row[0] == _time && row[1] == _date && row[2] == _sum && row[3] == _des && row[4] == "T")
                        {
                            dataGridView_ListTask.Rows.RemoveAt(choose_task);
                            LoginedUser.getInstance().User.userReminders.RemoveAt(i);
                            ListTaskReminders(LoginedUser.getInstance().User.userReminders);
                            for (int m = 0; m < LoginWindow.userList.Count; m++)
                            {
                                if (LoginWindow.userList[m].Username == LoginedUser.getInstance().User.Username)
                                {
                                    LoginWindow.userList[m] = LoginedUser.getInstance().User;
                                }
                            }
                            if (RemindersList.Count != 0)
                            {
                                RemindersList.Clear();
                            }
                            foreach (User user in LoginWindow.userList)
                            {
                                RemindersList.AddRange(user.userReminders);
                            }
                            Util.SaveCsv(RemindersList, RemindersFilePath);
                            Tb_Empty_Task();
                            choose_task = -1;
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("Task is Empty!");
                btn_task_delete.Enabled = false;
                btn_task_update.Enabled = false;
            }
        }

        private void dataGridView_ListMeet_CellClick(object sender, DataGridViewCellEventArgs e)    //update selected meet
        {
            if (LoginedUser.getInstance().User.userReminders.Count != 0)
            {
                selected_meet = e.RowIndex;
                DataGridViewRow row = dataGridView_ListMeet.Rows[selected_meet];

                maskedTB_meet_time.Text = row.Cells[0].Value.ToString();
                tb_meet_date.Text = row.Cells[1].Value.ToString();
                tb_meet_summary.Text = row.Cells[2].Value.ToString();
                tb_meet_description.Text = row.Cells[3].Value.ToString();

                for (int i = 0; i < LoginedUser.getInstance().User.userReminders.Count; i++)
                {
                    Reminders tmp = new Reminders(time, date, summary, description, reminder_type, whoEntered);
                    tmp = (Reminders)LoginedUser.getInstance().User.userReminders[i];
                    string[] rows = { tmp.Time, tmp.Date, tmp.Summary, tmp.Description, tmp.Reminder_type, tmp.WhoEntered };
                    string _time = dataGridView_ListMeet.Rows[selected_meet].Cells[0].Value.ToString();
                    string _date = dataGridView_ListMeet.Rows[selected_meet].Cells[1].Value.ToString();
                    string _sum = dataGridView_ListMeet.Rows[selected_meet].Cells[2].Value.ToString();
                    string _des = dataGridView_ListMeet.Rows[selected_meet].Cells[3].Value.ToString();
                    if (rows[0] == _time && rows[1] == _date && rows[2] == _sum && rows[3] == _des && rows[4] == "M")
                    {
                        index_meet = i;
                    }
                }
            }
            else
            {
                MessageBox.Show("Meeting is Empty!");
                btn_meet_delete.Enabled = false;
                btn_meet_update.Enabled = false;
            } 
        }

        private void btn_meet_update_Click(object sender, EventArgs e)
        {
            string time = maskedTB_meet_time.Text;
            string date = tb_meet_date.Text;
            string summary = tb_meet_summary.Text;
            string description = tb_meet_description.Text;
            string reminder_type = "M";

            Reminders reminder = new Reminders(time, date, summary, description, reminder_type, LoginedUser.getInstance().User.Username);
            if (selected_meet != -1)
            {
                LoginedUser.getInstance().User.userReminders[index_meet] = reminder;
                ListMeetReminders(LoginedUser.getInstance().User.userReminders);
                for (int i = 0; i < LoginWindow.userList.Count; i++)
                {
                    if (LoginWindow.userList[i].Username == LoginedUser.getInstance().User.Username)
                    {
                        LoginWindow.userList[i] = LoginedUser.getInstance().User;
                    }
                }
                if (RemindersList.Count != 0)
                {
                    RemindersList.Clear();
                }
                foreach (User user in LoginWindow.userList)
                {
                    RemindersList.AddRange(user.userReminders);
                }
                Util.SaveCsv(RemindersList, RemindersFilePath);
                Tb_Empty_Meet();
            }
            index_meet = -1;
            selected_meet = -1;
        }

        private void dataGridView_ListTask_CellClick(object sender, DataGridViewCellEventArgs e)        //update selected task
        {
            if (LoginedUser.getInstance().User.userReminders.Count != 0)
            {
                selected_task = e.RowIndex;
                DataGridViewRow row = dataGridView_ListTask.Rows[selected_task];

                maskedTB_task_time.Text = row.Cells[0].Value.ToString();
                tb_task_date.Text = row.Cells[1].Value.ToString();
                tb_task_summary.Text = row.Cells[2].Value.ToString();
                tb_task_description.Text = row.Cells[3].Value.ToString();

                for (int i = 0; i < LoginedUser.getInstance().User.userReminders.Count; i++)
                {
                    Reminders tmp = new Reminders(time, date, summary, description, reminder_type, whoEntered);
                    tmp = (Reminders)LoginedUser.getInstance().User.userReminders[i];
                    string[] rows = { tmp.Time, tmp.Date, tmp.Summary, tmp.Description, tmp.Reminder_type, tmp.WhoEntered };
                    string _time = dataGridView_ListTask.Rows[selected_task].Cells[0].Value.ToString();
                    string _date = dataGridView_ListTask.Rows[selected_task].Cells[1].Value.ToString();
                    string _sum = dataGridView_ListTask.Rows[selected_task].Cells[2].Value.ToString();
                    string _des = dataGridView_ListTask.Rows[selected_task].Cells[3].Value.ToString();
                    if (rows[0] == _time && rows[1] == _date && rows[2] == _sum && rows[3] == _des && rows[4] == "T")
                    {
                        index_task = i;
                    }
                }
            }
            else
            {
                MessageBox.Show("Task is Empty!");
                btn_task_delete.Enabled = false;
                btn_task_update.Enabled = false;
            }
        }

        private void btn_task_update_Click(object sender, EventArgs e)
        {
            string time = maskedTB_task_time.Text;
            string date = tb_task_date.Text;
            string summary = tb_task_summary.Text;
            string description = tb_task_description.Text;
            string reminder_type = "T";

            Reminders reminder = new Reminders(time, date, summary, description, reminder_type, LoginedUser.getInstance().User.Username);
            if (selected_task != -1)
            {
                LoginedUser.getInstance().User.userReminders[index_task] = reminder;
                ListTaskReminders(LoginedUser.getInstance().User.userReminders);
                for (int i = 0; i < LoginWindow.userList.Count; i++)
                {
                    if (LoginWindow.userList[i].Username == LoginedUser.getInstance().User.Username)
                    {
                        LoginWindow.userList[i] = LoginedUser.getInstance().User;
                    }
                }
                if (RemindersList.Count != 0)
                {
                    RemindersList.Clear();
                }
                foreach (User user in LoginWindow.userList)
                {
                    RemindersList.AddRange(user.userReminders);
                }
                Util.SaveCsv(RemindersList, RemindersFilePath);
                Tb_Empty_Task();
            }
            selected_task = -1;
            index_task = -1;
        }

        private void btn_task_add_Click(object sender, EventArgs e)
        {
            string time = maskedTB_task_time.Text;
            string date = tb_task_date.Text;
            string summary = tb_task_summary.Text;
            string description = tb_task_description.Text;
            string reminder_type = "T";

            if (time != "" && date != "" && summary != "" && description != "")
            {
                Reminders Reminders = new Reminders(time, date, summary, description, reminder_type, LoginedUser.getInstance().User.Username);
                LoginedUser.getInstance().User.userReminders.Add(Reminders);
                for (int i = 0; i < LoginWindow.userList.Count; i++)
                {
                    if (LoginWindow.userList[i].Username == LoginedUser.getInstance().User.Username)
                    {
                        LoginWindow.userList[i] = LoginedUser.getInstance().User;
                    }
                }
                if (RemindersList.Count != 0)
                {
                    RemindersList.Clear();
                }
                foreach (User user in LoginWindow.userList)
                {
                    RemindersList.AddRange(user.userReminders);
                }
                Util.SaveCsv(RemindersList, RemindersFilePath);
            }
            else
            {
                MessageBox.Show("Error ! ");
            }
            Tb_Empty_Task();
            timer_reminder_task.Start();
        }

        private void ListMeetReminders(List<Reminders> reminders)
        {
            dataGridView_ListMeet.Rows.Clear();
            for (int i = 0; i < reminders.Count; i++)
            {
                Reminders tmp = new Reminders(time, date, summary, description, reminder_type, whoEntered);
                tmp = (Reminders)reminders[i];
                string[] row = { tmp.Time, tmp.Date, tmp.Summary, tmp.Description, tmp.Reminder_type, tmp.WhoEntered };
                if (row[4] == "M")
                    dataGridView_ListMeet.Rows.Add(row);
            }
            btn_meet_delete.Enabled = true;
            btn_meet_update.Enabled = true;
        }

        private void ListTaskReminders(List<Reminders> reminders)
        {
            dataGridView_ListTask.Rows.Clear();
            for (int i = 0; i < reminders.Count; i++)
            {
                Reminders tmp = new Reminders(time, date, summary, description, reminder_type, whoEntered);
                tmp = (Reminders)reminders[i];
                string[] row = { tmp.Time, tmp.Date, tmp.Summary, tmp.Description, tmp.Reminder_type, tmp.WhoEntered };
                if (row[4] == "T")
                    dataGridView_ListTask.Rows.Add(row);
            }
            btn_task_delete.Enabled = true;
            btn_task_update.Enabled = true;
        }

        private void Reminders_Load(object sender, EventArgs e)
        {
            dataGridView_ListMeet.ColumnCount = 4;
            dataGridView_ListMeet.Columns[0].Name = "Time";
            dataGridView_ListMeet.Columns[1].Name = "Date";
            dataGridView_ListMeet.Columns[2].Name = "Summary";
            dataGridView_ListMeet.Columns[3].Name = "Description";
            dataGridView_ListTask.ColumnCount = 4;
            dataGridView_ListTask.Columns[0].Name = "Time";
            dataGridView_ListTask.Columns[1].Name = "Date";
            dataGridView_ListTask.Columns[2].Name = "Summary";
            dataGridView_ListTask.Columns[3].Name = "Description";
            Util.LoadRemindersCsv(RemindersFilePath);
            timer_RealTime.Enabled = true;
            btn_meet_delete.Enabled = false;
            btn_task_delete.Enabled = false;
            btn_meet_update.Enabled = false;
            btn_task_update.Enabled = false;
            dataGridView_ListMeet.Hide();
            dataGridView_ListTask.Hide();
        }

        private void Reminders_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void Tb_Empty_Meet()
        {
            maskedTB_meet_time.Text = "";
            tb_meet_date.Text = "";
            tb_meet_summary.Text = "";
            tb_meet_description.Text = "";
        }

        private void Tb_Empty_Task()
        {
            maskedTB_task_time.Text = "";
            tb_task_date.Text = "";
            tb_task_summary.Text = "";
            tb_task_description.Text = "";
        }
    }
   
}
