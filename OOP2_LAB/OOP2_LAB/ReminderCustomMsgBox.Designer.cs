﻿namespace OOP2_LAB
{
    partial class ReminderCustomMsgBox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel_text = new System.Windows.Forms.Panel();
            this.lbl_summary_txt = new System.Windows.Forms.Label();
            this.btn_OK = new System.Windows.Forms.Button();
            this.timer_CustomMsgBox = new System.Windows.Forms.Timer(this.components);
            this.panel_text.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel_text
            // 
            this.panel_text.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.panel_text.Controls.Add(this.lbl_summary_txt);
            this.panel_text.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel_text.Location = new System.Drawing.Point(0, 0);
            this.panel_text.Name = "panel_text";
            this.panel_text.Size = new System.Drawing.Size(720, 164);
            this.panel_text.TabIndex = 0;
            // 
            // lbl_summary_txt
            // 
            this.lbl_summary_txt.AutoSize = true;
            this.lbl_summary_txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl_summary_txt.Location = new System.Drawing.Point(21, 65);
            this.lbl_summary_txt.Name = "lbl_summary_txt";
            this.lbl_summary_txt.Size = new System.Drawing.Size(0, 25);
            this.lbl_summary_txt.TabIndex = 0;
            // 
            // btn_OK
            // 
            this.btn_OK.Location = new System.Drawing.Point(275, 202);
            this.btn_OK.Name = "btn_OK";
            this.btn_OK.Size = new System.Drawing.Size(128, 60);
            this.btn_OK.TabIndex = 1;
            this.btn_OK.Text = "OK";
            this.btn_OK.UseVisualStyleBackColor = true;
            this.btn_OK.Click += new System.EventHandler(this.btn_OK_Click);
            // 
            // timer_CustomMsgBox
            // 
            this.timer_CustomMsgBox.Tick += new System.EventHandler(this.timer_CustomMsgBox_Tick);
            // 
            // ReminderCustomMsgBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(720, 311);
            this.Controls.Add(this.btn_OK);
            this.Controls.Add(this.panel_text);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ReminderCustomMsgBox";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.ReminderCustomMsgBox_Load);
            this.panel_text.ResumeLayout(false);
            this.panel_text.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel_text;
        private System.Windows.Forms.Label lbl_summary_txt;
        private System.Windows.Forms.Button btn_OK;
        private System.Windows.Forms.Timer timer_CustomMsgBox;
    }
}