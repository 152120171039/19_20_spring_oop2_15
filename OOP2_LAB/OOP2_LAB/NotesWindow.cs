﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OOP2_LAB
{
    public partial class NotesWindow : Form
    {
        public static List<Notes> NotesList = new List<Notes>();
        public const string NotesFilePath = @"notes.csv";
        public static int selected_index = -1;
        string whoEntered, note;
        public NotesWindow()
        {
            InitializeComponent();
        }

        private void list2listview(List<Notes> nts)
        {
            listView_Notes.Items.Clear();
            for (int i = 0; i < nts.Count; i++)
            {
                Notes tmp = new Notes(note, LoginedUser.getInstance().User.Username);
                tmp = (Notes)nts[i];
                string[] row = { tmp.Note };
                ListViewItem lst_note = new ListViewItem(row);
                listView_Notes.Items.Add(lst_note);
            }
        }

        private void btn_create_Click(object sender, EventArgs e)
        {
            string note = tb_note.Text;
            if (note != "")
            {
                Notes Note = new Notes(note, LoginedUser.getInstance().User.Username);
                LoginedUser.getInstance().User.userNotes.Add(Note);
                list2listview(LoginedUser.getInstance().User.userNotes);
                for (int i = 0; i < LoginWindow.userList.Count; i++)
                {
                    if (LoginWindow.userList[i].Username == LoginedUser.getInstance().User.Username)
                    {
                        LoginWindow.userList[i] = LoginedUser.getInstance().User;
                    }
                }
                if (NotesList.Count != 0)
                {
                    NotesList.Clear();
                }
                foreach (User user in LoginWindow.userList)
                {
                    NotesList.AddRange(user.userNotes);
                }
                Util.SaveCsv(NotesList, NotesFilePath);
            }
            else
            {
                MessageBox.Show("Error ! ");    
            }
            tb_note.Text = "";
        }

        private void NotesWindow_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void btn_list_Click(object sender, EventArgs e)
        {
            
            list2listview(LoginedUser.getInstance().User.userNotes);
            listView_Notes.Show();
            tb_note.Text = "";
        }

        private void listView_Notes_SelectedIndexChanged(object sender, EventArgs e)
        {
            selected_index = listView_Notes.FocusedItem.Index;
            tb_note.Text = listView_Notes.Items[selected_index].SubItems[0].Text;
            btn_delete.Enabled = true;
            btn_update.Enabled = true;
        }

        private void btn_update_Click(object sender, EventArgs e)
        {
            string note = tb_note.Text;
            string whoEntered = LoginedUser.getInstance().User.Username;
            Notes notes = new Notes(note, whoEntered);
            if (selected_index != -1)
            {
                LoginedUser.getInstance().User.userNotes[selected_index] = notes;
                list2listview(LoginedUser.getInstance().User.userNotes);
                for(int i=0; i < LoginWindow.userList.Count; i++)
                {
                    if(LoginWindow.userList[i].Username == LoginedUser.getInstance().User.Username)
                    {
                        LoginWindow.userList[i] = LoginedUser.getInstance().User;
                    }
                }
                if(NotesList.Count!=0)
                {
                    NotesList.Clear();
                }
                foreach(User user in LoginWindow.userList)
                {
                    NotesList.AddRange(user.userNotes);
                }
                Util.SaveCsv(NotesList, NotesFilePath);
                tb_note.Text = "";
            }
            selected_index = -1;
            btn_delete.Enabled = false;
            btn_update.Enabled = false;
        }

        private void btn_delete_Click(object sender, EventArgs e)
        {
            if (LoginedUser.getInstance().User.userNotes.Count != 0)
            {
                DialogResult result = MessageBox.Show("Do you want to delete the contact ? ", "Deleting...", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (result == DialogResult.Yes)
                {
                    LoginedUser.getInstance().User.userNotes.RemoveAt(selected_index);
                    list2listview(LoginedUser.getInstance().User.userNotes);
                    for (int i = 0; i < LoginWindow.userList.Count; i++)
                    {
                        if (LoginWindow.userList[i].Username == LoginedUser.getInstance().User.Username)
                        {
                            LoginWindow.userList[i] = LoginedUser.getInstance().User;
                        }
                    }
                    if (NotesList.Count != 0)
                    {
                        NotesList.Clear();
                    }
                    foreach (User user in LoginWindow.userList)
                    {
                        NotesList.AddRange(user.userNotes);
                    }
                    Util.SaveCsv(NotesList, NotesFilePath);
                    tb_note.Text = "";
                    selected_index = -1;
                    btn_delete.Enabled = false;
                    btn_update.Enabled = false;
                }
            }
            else
            {
                MessageBox.Show("No contacat found...");
            }
        }

        private void btn_mainwindow_Click(object sender, EventArgs e)
        {
            this.Hide();
            MainWindow mainWindow = new MainWindow();
            mainWindow.Show();
        }

        private void NotesWindow_Load(object sender, EventArgs e)
        {
            listView_Notes.Columns.Add("NOTE", 500);
            Util.LoadNotesCsv(NotesFilePath);
            btn_update.Enabled = false;
            btn_delete.Enabled = false;
            listView_Notes.Hide();
        }
    }
}
