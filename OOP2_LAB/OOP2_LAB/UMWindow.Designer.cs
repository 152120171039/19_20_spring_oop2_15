﻿namespace OOP2_LAB
{
    partial class UMWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lboxUserList = new System.Windows.Forms.ListBox();
            this.lbUserlist = new System.Windows.Forms.Label();
            this.lbWarningSelect = new System.Windows.Forms.Label();
            this.lbSelectedUser = new System.Windows.Forms.Label();
            this.lbUsername = new System.Windows.Forms.Label();
            this.lbUserType = new System.Windows.Forms.Label();
            this.txtUsername = new System.Windows.Forms.TextBox();
            this.lboxUserType = new System.Windows.Forms.ListBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnMainWindow = new System.Windows.Forms.Button();
            this.lblSalary = new System.Windows.Forms.Label();
            this.txtSalary = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lboxUserList
            // 
            this.lboxUserList.FormattingEnabled = true;
            this.lboxUserList.Location = new System.Drawing.Point(12, 48);
            this.lboxUserList.Name = "lboxUserList";
            this.lboxUserList.Size = new System.Drawing.Size(120, 147);
            this.lboxUserList.TabIndex = 0;
            this.lboxUserList.SelectedIndexChanged += new System.EventHandler(this.lboxUserList_SelectedIndexChanged);
            // 
            // lbUserlist
            // 
            this.lbUserlist.AutoSize = true;
            this.lbUserlist.Location = new System.Drawing.Point(37, 32);
            this.lbUserlist.Name = "lbUserlist";
            this.lbUserlist.Size = new System.Drawing.Size(65, 13);
            this.lbUserlist.TabIndex = 1;
            this.lbUserlist.Text = "List of Users";
            // 
            // lbWarningSelect
            // 
            this.lbWarningSelect.AutoSize = true;
            this.lbWarningSelect.Location = new System.Drawing.Point(9, 210);
            this.lbWarningSelect.Name = "lbWarningSelect";
            this.lbWarningSelect.Size = new System.Drawing.Size(158, 13);
            this.lbWarningSelect.TabIndex = 2;
            this.lbWarningSelect.Text = "Select a user to see information.";
            // 
            // lbSelectedUser
            // 
            this.lbSelectedUser.AutoSize = true;
            this.lbSelectedUser.Location = new System.Drawing.Point(285, 32);
            this.lbSelectedUser.Name = "lbSelectedUser";
            this.lbSelectedUser.Size = new System.Drawing.Size(129, 13);
            this.lbSelectedUser.TabIndex = 3;
            this.lbSelectedUser.Text = "Selected User Information";
            this.lbSelectedUser.Visible = false;
            // 
            // lbUsername
            // 
            this.lbUsername.AutoSize = true;
            this.lbUsername.Location = new System.Drawing.Point(254, 76);
            this.lbUsername.Name = "lbUsername";
            this.lbUsername.Size = new System.Drawing.Size(55, 13);
            this.lbUsername.TabIndex = 4;
            this.lbUsername.Text = "Username";
            this.lbUsername.Visible = false;
            // 
            // lbUserType
            // 
            this.lbUserType.AutoSize = true;
            this.lbUserType.Location = new System.Drawing.Point(393, 76);
            this.lbUserType.Name = "lbUserType";
            this.lbUserType.Size = new System.Drawing.Size(56, 13);
            this.lbUserType.TabIndex = 5;
            this.lbUserType.Text = "User Type";
            this.lbUserType.Visible = false;
            // 
            // txtUsername
            // 
            this.txtUsername.Location = new System.Drawing.Point(225, 92);
            this.txtUsername.Name = "txtUsername";
            this.txtUsername.ReadOnly = true;
            this.txtUsername.Size = new System.Drawing.Size(131, 20);
            this.txtUsername.TabIndex = 6;
            this.txtUsername.Visible = false;
            // 
            // lboxUserType
            // 
            this.lboxUserType.FormattingEnabled = true;
            this.lboxUserType.Location = new System.Drawing.Point(362, 92);
            this.lboxUserType.Name = "lboxUserType";
            this.lboxUserType.Size = new System.Drawing.Size(120, 95);
            this.lboxUserType.TabIndex = 7;
            this.lboxUserType.Visible = false;
            this.lboxUserType.SelectedIndexChanged += new System.EventHandler(this.lboxUserType_SelectedIndexChanged);
            // 
            // btnSave
            // 
            this.btnSave.Enabled = false;
            this.btnSave.Location = new System.Drawing.Point(319, 205);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(130, 23);
            this.btnSave.TabIndex = 9;
            this.btnSave.Text = "Save Changes";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Visible = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnMainWindow
            // 
            this.btnMainWindow.Location = new System.Drawing.Point(362, 346);
            this.btnMainWindow.Name = "btnMainWindow";
            this.btnMainWindow.Size = new System.Drawing.Size(143, 23);
            this.btnMainWindow.TabIndex = 10;
            this.btnMainWindow.Text = "Go Back to Main Window";
            this.btnMainWindow.UseVisualStyleBackColor = true;
            this.btnMainWindow.Click += new System.EventHandler(this.btnMainWindow_Click);
            // 
            // lblSalary
            // 
            this.lblSalary.AutoSize = true;
            this.lblSalary.Location = new System.Drawing.Point(254, 126);
            this.lblSalary.Name = "lblSalary";
            this.lblSalary.Size = new System.Drawing.Size(36, 13);
            this.lblSalary.TabIndex = 11;
            this.lblSalary.Text = "Salary";
            // 
            // txtSalary
            // 
            this.txtSalary.Location = new System.Drawing.Point(225, 142);
            this.txtSalary.Name = "txtSalary";
            this.txtSalary.Size = new System.Drawing.Size(131, 20);
            this.txtSalary.TabIndex = 12;
            // 
            // UMWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(638, 381);
            this.Controls.Add(this.txtSalary);
            this.Controls.Add(this.lblSalary);
            this.Controls.Add(this.btnMainWindow);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.lboxUserType);
            this.Controls.Add(this.txtUsername);
            this.Controls.Add(this.lbUserType);
            this.Controls.Add(this.lbUsername);
            this.Controls.Add(this.lbSelectedUser);
            this.Controls.Add(this.lbWarningSelect);
            this.Controls.Add(this.lbUserlist);
            this.Controls.Add(this.lboxUserList);
            this.Name = "UMWindow";
            this.Text = "UMWindow";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.UMWindow_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lboxUserList;
        private System.Windows.Forms.Label lbUserlist;
        private System.Windows.Forms.Label lbWarningSelect;
        private System.Windows.Forms.Label lbSelectedUser;
        private System.Windows.Forms.Label lbUsername;
        private System.Windows.Forms.Label lbUserType;
        private System.Windows.Forms.TextBox txtUsername;
        private System.Windows.Forms.ListBox lboxUserType;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnMainWindow;
        private System.Windows.Forms.Label lblSalary;
        private System.Windows.Forms.TextBox txtSalary;
    }
}