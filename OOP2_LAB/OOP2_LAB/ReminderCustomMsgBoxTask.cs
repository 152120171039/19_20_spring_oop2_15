﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OOP2_LAB
{
    public partial class ReminderCustomMsgBoxTask : Form
    {
        public ReminderCustomMsgBoxTask()
        {
            InitializeComponent();
        }

        public static int ShakeCount = 0;
        static ReminderCustomMsgBoxTask MsgBoxtask;
        static DialogResult result = DialogResult.OK;
        public static DialogResult Show(string text, string caption)
        {
            MsgBoxtask = new ReminderCustomMsgBoxTask();
            MsgBoxtask.lbl_task_msgbx.Text = text;
            MsgBoxtask.Text = caption;
            MsgBoxtask.ShowDialog();
            return result;
        }

        private void ReminderCustomMsgBoxTask_Load(object sender, EventArgs e)
        {
            timer_MsgBox_task.Start();
        }

        private void button_task_OK_Click(object sender, EventArgs e)
        {
            result = DialogResult.OK;
            MsgBoxtask.Close();
        }

        public static void Shake(Form form)
        {
            var original = form.Location;
            var rnd = new Random(1337);
            const int shake_amplitude = 10;
            for (int i = 0; i < 70; i++)
            {
                form.Location = new Point(original.X + rnd.Next(-shake_amplitude, shake_amplitude), original.Y + rnd.Next(-shake_amplitude, shake_amplitude));
                System.Threading.Thread.Sleep(20);
                form.Location = original;
            }
        }

        private void timer_MsgBox_task_Tick(object sender, EventArgs e)
        {
            timer_MsgBox_task.Stop();
            Shake(this);
        }
    }
}
