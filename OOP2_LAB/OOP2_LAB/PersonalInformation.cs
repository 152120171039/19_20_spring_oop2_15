﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP2_LAB
{
    public class PersonalInformation
    {
        private string name;
        private string surname;
        private string phoneNumber;
        private string address;
        private string mail;
        private string photoBase64;
        private string _salary;

        public PersonalInformation()
        {
            name = "Name";
            surname = "Surname";
            phoneNumber = "(999) 999-9999";
            address = "Address";
            mail = "mail@mail.com";
            photoBase64 = "No photo";
            _salary = "0.0";
        }

        public PersonalInformation(string name, string surname, string phoneNumber, string address, string mail,string photoBase64,string _salary)
        {
            this.name = name;
            this.surname = surname;
            this.phoneNumber = phoneNumber;
            this.address = address;
            this.mail = mail;
            this.photoBase64 = photoBase64;
            this._salary = _salary;
        }

        public string Name { get => name; set => name = value; }
        public string Surname { get => surname; set => surname = value; }
        public string PhoneNumber { get => phoneNumber; set => phoneNumber = value; }
        public string Address { get => address; set => address = value; }
        public string Mail { get => mail; set => mail = value; }
        public string PhotoBase64 { get => photoBase64; set => photoBase64 = value; }
        public string Salary_ { get => _salary; set => _salary = value; }
        public string ToString()
        {
            return name + "," + surname + "," + phoneNumber + "," + address + "," + mail + "," + photoBase64 + "," + _salary;
        }
        public Memento Save()
        {
            return new Memento
            {
                name = Name,
                surname = Surname,
                phoneNumber = PhoneNumber,
                address = Address,
                photoBase64 = PhotoBase64,
                mail = Mail,
            };
        }
        public void Undo(Memento memento)
        {
            this.Name = memento.name;
            this.Surname = memento.surname;
            this.PhoneNumber = memento.phoneNumber;
            this.Address = memento.address;
            this.PhotoBase64 = memento.photoBase64;
            this.Mail = memento.mail;
        }
    }

    public class Memento
    {
        public string name { get; set; }
        public string surname { get; set; }
        public string phoneNumber { get; set; }
        public string address { get; set; }
        public string photoBase64 { get; set; }
        public string mail { get; set; }
    }

    public class CareTakerDatas
    {
        public Memento Memento { get; set; }
    }
}
