﻿namespace OOP2_LAB
{
    partial class PIWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tbName = new System.Windows.Forms.TextBox();
            this.tbSurname = new System.Windows.Forms.TextBox();
            this.tbAddress = new System.Windows.Forms.TextBox();
            this.tbPassword = new System.Windows.Forms.TextBox();
            this.lbName = new System.Windows.Forms.Label();
            this.lbSurname = new System.Windows.Forms.Label();
            this.lbAddress = new System.Windows.Forms.Label();
            this.lbMail = new System.Windows.Forms.Label();
            this.lbPhone = new System.Windows.Forms.Label();
            this.lbPassword = new System.Windows.Forms.Label();
            this.tbMail = new System.Windows.Forms.TextBox();
            this.pboxUser = new System.Windows.Forms.PictureBox();
            this.lbPhoto = new System.Windows.Forms.Label();
            this.lbUserInfo = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.toolTipPhoto = new System.Windows.Forms.ToolTip(this.components);
            this.tbPhone = new System.Windows.Forms.MaskedTextBox();
            this.btnBack = new System.Windows.Forms.Button();
            this.lbl_salary = new System.Windows.Forms.Label();
            this.tb_salary = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pboxUser)).BeginInit();
            this.SuspendLayout();
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(148, 244);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(180, 20);
            this.tbName.TabIndex = 0;
            this.tbName.TextChanged += new System.EventHandler(this.tbName_TextChanged);
            // 
            // tbSurname
            // 
            this.tbSurname.Location = new System.Drawing.Point(148, 280);
            this.tbSurname.Name = "tbSurname";
            this.tbSurname.Size = new System.Drawing.Size(180, 20);
            this.tbSurname.TabIndex = 2;
            this.tbSurname.TextChanged += new System.EventHandler(this.tbSurname_TextChanged);
            // 
            // tbAddress
            // 
            this.tbAddress.Location = new System.Drawing.Point(148, 357);
            this.tbAddress.Name = "tbAddress";
            this.tbAddress.Size = new System.Drawing.Size(180, 20);
            this.tbAddress.TabIndex = 4;
            this.tbAddress.TextChanged += new System.EventHandler(this.tbAddress_TextChanged);
            // 
            // tbPassword
            // 
            this.tbPassword.Location = new System.Drawing.Point(148, 432);
            this.tbPassword.Name = "tbPassword";
            this.tbPassword.ReadOnly = true;
            this.tbPassword.Size = new System.Drawing.Size(180, 20);
            this.tbPassword.TabIndex = 5;
            this.tbPassword.Text = "********";
            this.tbPassword.UseSystemPasswordChar = true;
            this.tbPassword.Click += new System.EventHandler(this.tbPassword_Click);
            this.tbPassword.TextChanged += new System.EventHandler(this.tbPassword_TextChanged);
            // 
            // lbName
            // 
            this.lbName.AutoSize = true;
            this.lbName.Location = new System.Drawing.Point(107, 247);
            this.lbName.Name = "lbName";
            this.lbName.Size = new System.Drawing.Size(35, 13);
            this.lbName.TabIndex = 7;
            this.lbName.Text = "Name";
            // 
            // lbSurname
            // 
            this.lbSurname.AutoSize = true;
            this.lbSurname.Location = new System.Drawing.Point(93, 283);
            this.lbSurname.Name = "lbSurname";
            this.lbSurname.Size = new System.Drawing.Size(49, 13);
            this.lbSurname.TabIndex = 8;
            this.lbSurname.Text = "Surname";
            // 
            // lbAddress
            // 
            this.lbAddress.AutoSize = true;
            this.lbAddress.Location = new System.Drawing.Point(97, 360);
            this.lbAddress.Name = "lbAddress";
            this.lbAddress.Size = new System.Drawing.Size(45, 13);
            this.lbAddress.TabIndex = 9;
            this.lbAddress.Text = "Address";
            // 
            // lbMail
            // 
            this.lbMail.AutoSize = true;
            this.lbMail.Location = new System.Drawing.Point(106, 399);
            this.lbMail.Name = "lbMail";
            this.lbMail.Size = new System.Drawing.Size(36, 13);
            this.lbMail.TabIndex = 10;
            this.lbMail.Text = "E-Mail";
            // 
            // lbPhone
            // 
            this.lbPhone.AutoSize = true;
            this.lbPhone.Location = new System.Drawing.Point(64, 321);
            this.lbPhone.Name = "lbPhone";
            this.lbPhone.Size = new System.Drawing.Size(78, 13);
            this.lbPhone.TabIndex = 11;
            this.lbPhone.Text = "Phone Number";
            // 
            // lbPassword
            // 
            this.lbPassword.AutoSize = true;
            this.lbPassword.Location = new System.Drawing.Point(89, 435);
            this.lbPassword.Name = "lbPassword";
            this.lbPassword.Size = new System.Drawing.Size(53, 13);
            this.lbPassword.TabIndex = 12;
            this.lbPassword.Text = "Password";
            // 
            // tbMail
            // 
            this.tbMail.Location = new System.Drawing.Point(148, 396);
            this.tbMail.Name = "tbMail";
            this.tbMail.Size = new System.Drawing.Size(180, 20);
            this.tbMail.TabIndex = 13;
            this.tbMail.TextChanged += new System.EventHandler(this.tbMail_TextChanged);
            // 
            // pboxUser
            // 
            this.pboxUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pboxUser.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pboxUser.Location = new System.Drawing.Point(174, 72);
            this.pboxUser.Name = "pboxUser";
            this.pboxUser.Size = new System.Drawing.Size(134, 137);
            this.pboxUser.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pboxUser.TabIndex = 6;
            this.pboxUser.TabStop = false;
            this.pboxUser.Click += new System.EventHandler(this.pboxUser_Click);
            this.pboxUser.MouseHover += new System.EventHandler(this.pboxUser_MouseHover);
            // 
            // lbPhoto
            // 
            this.lbPhoto.AutoSize = true;
            this.lbPhoto.Location = new System.Drawing.Point(220, 212);
            this.lbPhoto.Name = "lbPhoto";
            this.lbPhoto.Size = new System.Drawing.Size(35, 13);
            this.lbPhoto.TabIndex = 14;
            this.lbPhoto.Text = "Photo";
            // 
            // lbUserInfo
            // 
            this.lbUserInfo.AutoSize = true;
            this.lbUserInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbUserInfo.Location = new System.Drawing.Point(159, 30);
            this.lbUserInfo.Name = "lbUserInfo";
            this.lbUserInfo.Size = new System.Drawing.Size(0, 20);
            this.lbUserInfo.TabIndex = 15;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(194, 513);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 16;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // tbPhone
            // 
            this.tbPhone.Location = new System.Drawing.Point(148, 318);
            this.tbPhone.Mask = "(999) 000-0000";
            this.tbPhone.Name = "tbPhone";
            this.tbPhone.Size = new System.Drawing.Size(180, 20);
            this.tbPhone.TabIndex = 17;
            this.tbPhone.TextChanged += new System.EventHandler(this.tbPhone_TextChanged);
            // 
            // btnBack
            // 
            this.btnBack.Location = new System.Drawing.Point(369, 541);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(75, 55);
            this.btnBack.TabIndex = 18;
            this.btnBack.Text = "Back to Main Window";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // lbl_salary
            // 
            this.lbl_salary.AutoSize = true;
            this.lbl_salary.Location = new System.Drawing.Point(107, 477);
            this.lbl_salary.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_salary.Name = "lbl_salary";
            this.lbl_salary.Size = new System.Drawing.Size(39, 13);
            this.lbl_salary.TabIndex = 19;
            this.lbl_salary.Text = "Salary ";
            // 
            // tb_salary
            // 
            this.tb_salary.Location = new System.Drawing.Point(148, 473);
            this.tb_salary.Margin = new System.Windows.Forms.Padding(2);
            this.tb_salary.Name = "tb_salary";
            this.tb_salary.Size = new System.Drawing.Size(180, 20);
            this.tb_salary.TabIndex = 20;
            // 
            // PIWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(456, 608);
            this.Controls.Add(this.tb_salary);
            this.Controls.Add(this.lbl_salary);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.tbPhone);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.lbUserInfo);
            this.Controls.Add(this.lbPhoto);
            this.Controls.Add(this.tbMail);
            this.Controls.Add(this.lbPassword);
            this.Controls.Add(this.lbPhone);
            this.Controls.Add(this.lbMail);
            this.Controls.Add(this.lbAddress);
            this.Controls.Add(this.lbSurname);
            this.Controls.Add(this.lbName);
            this.Controls.Add(this.pboxUser);
            this.Controls.Add(this.tbPassword);
            this.Controls.Add(this.tbAddress);
            this.Controls.Add(this.tbSurname);
            this.Controls.Add(this.tbName);
            this.Name = "PIWindow";
            this.Text = "PIWindow";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.PIWindow_FormClosing);
            this.Load += new System.EventHandler(this.PIWindow_Load);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.PIWindow_KeyPress);
            ((System.ComponentModel.ISupportInitialize)(this.pboxUser)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.TextBox tbSurname;
        private System.Windows.Forms.TextBox tbAddress;
        private System.Windows.Forms.TextBox tbPassword;
        private System.Windows.Forms.PictureBox pboxUser;
        private System.Windows.Forms.Label lbName;
        private System.Windows.Forms.Label lbSurname;
        private System.Windows.Forms.Label lbAddress;
        private System.Windows.Forms.Label lbMail;
        private System.Windows.Forms.Label lbPhone;
        private System.Windows.Forms.Label lbPassword;
        private System.Windows.Forms.TextBox tbMail;
        private System.Windows.Forms.Label lbPhoto;
        private System.Windows.Forms.Label lbUserInfo;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.ToolTip toolTipPhoto;
        private System.Windows.Forms.MaskedTextBox tbPhone;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Label lbl_salary;
        private System.Windows.Forms.TextBox tb_salary;
    }
}