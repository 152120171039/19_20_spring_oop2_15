﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OOP2_LAB
{
    public partial class LoginWindow : Form
    {
        public static List<User> userList = new List<User>();
        public const string userFilePath = @"mydb.csv";

        public LoginWindow()
        {
            InitializeComponent();
            if (userList.Count != 0)
            {
                userList.Clear();
            }
            Util.LoadCsv(userList, userFilePath);
            if (userList.Count == 0) 
            {
                lbAccountAttention.Text = "Please create first account of program!";
                lbAccountAttention.BackColor = Color.Red;
                lbAccountAttention.ForeColor = Color.White;
                tbUsername.ReadOnly = true;
                tbPassword.ReadOnly = true;
                btnLogin.Enabled = false;
            }
            else
            {
                lbAccountAttention.Text = "";
                tbUsername.ReadOnly = false;
                tbUsername.ReadOnly = false;
                btnLogin.Enabled = true;
            }
            checkRememberedUser();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < userList.Count; i++)
            {
                User user = userList[i];
                string username = tbUsername.Text;
                string password = tbPassword.Text;
                bool rememberMe = checkRememberMe.Checked;
                if (user.IsValid(username, password))
                {
                    user.RememberMe = rememberMe;
                    LoginedUser.getInstance().User = user;
                    lbLoginMessage.Text = "Success";
                    lbLoginMessage.ForeColor = Color.Green;
                    loginDelay.Start();
                    Util.SaveCsv(userList, userFilePath);
                    return;
                }
            }
            lbLoginMessage.Text = "Failure";
            lbLoginMessage.ForeColor = Color.Red;

        }

        private void loginDelay_Tick(object sender, EventArgs e)
        {
            login();
        }

        private void login()
        {
            this.Hide();
            MainWindow mainWindow = new MainWindow();
            mainWindow.Show();
            loginDelay.Stop();
        }

        private void checkRememberedUser()
        {
            foreach (User user in userList)
            {
                if (user.RememberMe)
                {
                    LoginedUser.getInstance().User = user;
                    loginDelay.Interval = 10;
                    loginDelay.Start();
                    return;
                }
            }
        }

        private void btnCreateAccount_Click(object sender, EventArgs e)
        {
            this.Hide();
            CreateAccountWindow createAccountWindow = new CreateAccountWindow();
            createAccountWindow.Show();
        }

        private void LoginWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
    }
}
