﻿namespace OOP2_LAB
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnLogout = new System.Windows.Forms.Button();
            this.btnUserManagement = new System.Windows.Forms.Button();
            this.btn_Phone_book = new System.Windows.Forms.Button();
            this.btn_Notes = new System.Windows.Forms.Button();
            this.btnPI = new System.Windows.Forms.Button();
            this.btn_salary = new System.Windows.Forms.Button();
            this.btn_reminders = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnLogout
            // 
            this.btnLogout.Location = new System.Drawing.Point(643, 500);
            this.btnLogout.Margin = new System.Windows.Forms.Padding(4);
            this.btnLogout.Name = "btnLogout";
            this.btnLogout.Size = new System.Drawing.Size(100, 28);
            this.btnLogout.TabIndex = 0;
            this.btnLogout.Text = "Logout";
            this.btnLogout.UseVisualStyleBackColor = true;
            this.btnLogout.Click += new System.EventHandler(this.btnLogout_Click);
            // 
            // btnUserManagement
            // 
            this.btnUserManagement.Location = new System.Drawing.Point(16, 485);
            this.btnUserManagement.Margin = new System.Windows.Forms.Padding(4);
            this.btnUserManagement.Name = "btnUserManagement";
            this.btnUserManagement.Size = new System.Drawing.Size(100, 54);
            this.btnUserManagement.TabIndex = 1;
            this.btnUserManagement.Text = "Manage Users";
            this.btnUserManagement.UseVisualStyleBackColor = true;
            this.btnUserManagement.Visible = false;
            this.btnUserManagement.Click += new System.EventHandler(this.btnUserManagement_Click);
            // 
            // btn_Phone_book
            // 
            this.btn_Phone_book.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btn_Phone_book.Location = new System.Drawing.Point(16, 36);
            this.btn_Phone_book.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_Phone_book.Name = "btn_Phone_book";
            this.btn_Phone_book.Size = new System.Drawing.Size(100, 46);
            this.btn_Phone_book.TabIndex = 2;
            this.btn_Phone_book.Text = "Phone Book";
            this.btn_Phone_book.UseVisualStyleBackColor = true;
            this.btn_Phone_book.Click += new System.EventHandler(this.btn_Phone_book_Click);
            // 
            // btn_Notes
            // 
            this.btn_Notes.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btn_Notes.Location = new System.Drawing.Point(213, 36);
            this.btn_Notes.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_Notes.Name = "btn_Notes";
            this.btn_Notes.Size = new System.Drawing.Size(100, 46);
            this.btn_Notes.TabIndex = 3;
            this.btn_Notes.Text = "Notes";
            this.btn_Notes.UseVisualStyleBackColor = true;
            this.btn_Notes.Click += new System.EventHandler(this.btn_Notes_Click);
            // 
            // btnPI
            // 
            this.btnPI.Location = new System.Drawing.Point(320, 36);
            this.btnPI.Margin = new System.Windows.Forms.Padding(4);
            this.btnPI.Name = "btnPI";
            this.btnPI.Size = new System.Drawing.Size(100, 46);
            this.btnPI.TabIndex = 4;
            this.btnPI.Text = "Personal Information";
            this.btnPI.UseVisualStyleBackColor = true;
            this.btnPI.Click += new System.EventHandler(this.btnPI_Click);
            // 
            // btn_salary
            // 
            this.btn_salary.Location = new System.Drawing.Point(427, 36);
            this.btn_salary.Name = "btn_salary";
            this.btn_salary.Size = new System.Drawing.Size(100, 46);
            this.btn_salary.TabIndex = 5;
            this.btn_salary.Text = "Salary Calculator";
            this.btn_salary.UseVisualStyleBackColor = true;
            this.btn_salary.Click += new System.EventHandler(this.btn_salary_Click);
            // 
            // btn_reminders
            // 
            this.btn_reminders.Location = new System.Drawing.Point(533, 36);
            this.btn_reminders.Name = "btn_reminders";
            this.btn_reminders.Size = new System.Drawing.Size(100, 46);
            this.btn_reminders.TabIndex = 6;
            this.btn_reminders.Text = "Reminders";
            this.btn_reminders.UseVisualStyleBackColor = true;
            this.btn_reminders.Click += new System.EventHandler(this.btn_reminders_Click);
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(779, 554);
            this.Controls.Add(this.btn_reminders);
            this.Controls.Add(this.btn_salary);
            this.Controls.Add(this.btnPI);
            this.Controls.Add(this.btn_Notes);
            this.Controls.Add(this.btn_Phone_book);
            this.Controls.Add(this.btnUserManagement);
            this.Controls.Add(this.btnLogout);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "MainWindow";
            this.Text = "MainWindow";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainWindow_FormClosing);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnLogout;
        private System.Windows.Forms.Button btnUserManagement;
        private System.Windows.Forms.Button btn_Phone_book;
        private System.Windows.Forms.Button btn_Notes;
        private System.Windows.Forms.Button btnPI;
        private System.Windows.Forms.Button btn_salary;
        private System.Windows.Forms.Button btn_reminders;
    }
}