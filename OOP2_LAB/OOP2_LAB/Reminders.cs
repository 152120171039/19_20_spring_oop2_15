﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP2_LAB
{
    public class Reminders
    {
        private String time;
        private String date;
        private String summary;
        private String description;
        private String reminder_type;
        private String whoEntered;

        public Reminders(String time,String date,String summary,String description,String reminder_type, String whoEntered)
        {
            this.time = time;
            this.date = date;
            this.summary = summary;
            this.description = description;
            this.reminder_type = reminder_type;
            this.whoEntered = whoEntered;
        }

        public string Time { get => time; set => time = value; }
        public string Date { get => date; set => date = value; }
        public string Summary { get => summary; set => summary = value; }
        public string Description { get => description; set => description = value; }
        public string Reminder_type { get => reminder_type; set => reminder_type = value; }
        public string WhoEntered { get => whoEntered; set => whoEntered = value; }

        public bool IsValid(string time,string date,string summary, string description)
        {
            return this.time.Equals(time) && this.date.Equals(date) && this.summary.Equals(summary) && this.description.Equals(description) && this.reminder_type.Equals(reminder_type);
        }

        public string ToString_save()
        {
            return Time + "," + Date + "," + Summary.Replace(System.Environment.NewLine, "\\n") + "," + Description.Replace(System.Environment.NewLine, "\\n") + "," + Reminder_type + "," + WhoEntered;
        }
    }
}
