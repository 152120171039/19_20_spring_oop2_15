﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OOP2_LAB
{
    public partial class PhoneBookWindow : Form
    {
        public static List<PhoneBook> PhoneBookList = new List<PhoneBook>();
        public const string PhoneBookFilePath = @"PhoneBook.csv";
        string name, surname, phone_number, address, description, e_mail;
        public static int selected_index = -1;  //update;
        public PhoneBookWindow()
        {
            InitializeComponent();
        }

        private void listView_phone_book_SelectedIndexChanged(object sender, EventArgs e)   //update için gerekli fonksiyon
        {
            selected_index = listView_phone_book.FocusedItem.Index;
            tb_name.Text = listView_phone_book.Items[selected_index].SubItems[0].Text;
            tb_surname.Text = listView_phone_book.Items[selected_index].SubItems[1].Text;
            tb_phone_number.Text = listView_phone_book.Items[selected_index].SubItems[2].Text;
            tb_address.Text = listView_phone_book.Items[selected_index].SubItems[3].Text;
            tb_description.Text = listView_phone_book.Items[selected_index].SubItems[4].Text;
            tb_e_mail.Text = listView_phone_book.Items[selected_index].SubItems[5].Text;
            btn_update.Enabled = true;
            btn_delete.Enabled = true;
        }

        private void btn_update_Click(object sender, EventArgs e)   //update butonu 
        {
            string name = tb_name.Text;
            string surname = tb_surname.Text;
            string phone_number = tb_phone_number.Text;
            string address = tb_address.Text;
            string description = tb_description.Text;
            string e_mail = tb_e_mail.Text;
            string whoEntered = LoginedUser.getInstance().User.Username;
            PhoneBook Phone_book = new PhoneBook(name, surname, phone_number, address, description, e_mail, whoEntered);
            if (selected_index!=-1)
            {
                LoginedUser.getInstance().User.userPhoneBook[selected_index] = Phone_book;
                list2listview(LoginedUser.getInstance().User.userPhoneBook);
                for (int i = 0; i < LoginWindow.userList.Count; i++)
                {
                    if (LoginWindow.userList[i].Username == LoginedUser.getInstance().User.Username)
                    {
                        LoginWindow.userList[i] = LoginedUser.getInstance().User;
                    }
                }
                if (PhoneBookList.Count != 0)
                {
                    PhoneBookList.Clear();
                }
                foreach (User user in LoginWindow.userList)
                {
                    PhoneBookList.AddRange(user.userPhoneBook);
                }
                Util.SaveCsv(PhoneBookList, PhoneBookFilePath);
                Tb_empty();
            }
            selected_index = -1;
            btn_delete.Enabled = false;
            btn_update.Enabled = false;
           
        }

        private void btn_delete_Click(object sender, EventArgs e)
        {
            if (LoginedUser.getInstance().User.userPhoneBook.Count!=0)
            {
                DialogResult result = MessageBox.Show("Do you want to delete the contact ? ", "Deleting...", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (result == DialogResult.Yes)
                {
                    LoginedUser.getInstance().User.userPhoneBook.RemoveAt(selected_index);
                    list2listview(LoginedUser.getInstance().User.userPhoneBook);
                    for (int i = 0; i < LoginWindow.userList.Count; i++)
                    {
                        if (LoginWindow.userList[i].Username == LoginedUser.getInstance().User.Username)
                        {
                            LoginWindow.userList[i] = LoginedUser.getInstance().User;
                        }
                    }
                    if (PhoneBookList.Count != 0)
                    {
                        PhoneBookList.Clear();
                    }
                    foreach (User user in LoginWindow.userList)
                    {
                        PhoneBookList.AddRange(user.userPhoneBook);
                    }
                    Util.SaveCsv(PhoneBookList, PhoneBookFilePath);
                    Tb_empty();
                    btn_delete.Enabled = false;
                    btn_update.Enabled = false;
                    selected_index = -1;
                }
            }
            else
            {
                MessageBox.Show("No contact found...");
            }
            
        }

        private void PhoneBookWindow_Load(object sender, EventArgs e)
        {
           
            listView_phone_book.Columns.Add("NAME", 90);
            listView_phone_book.Columns.Add("SURNAME", 90);
            listView_phone_book.Columns.Add("PHONE NUMBER", 100);
            listView_phone_book.Columns.Add("ADDRESS", 90);
            listView_phone_book.Columns.Add("DESCRIPTION", 100);
            listView_phone_book.Columns.Add("E-MAİL", 90);
            Util.LoadCsv(PhoneBookFilePath);
            btn_delete.Enabled = false;
            btn_update.Enabled = false;
            listView_phone_book.Hide();
        }

        private void tb_e_mail_Validating(object sender, CancelEventArgs e)
        {
            System.Text.RegularExpressions.Regex remail = new System.Text.RegularExpressions.Regex(@"^[a-zA-Z][\w\.-]{2,28}[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$");
            if (tb_e_mail.Text.Length > 0) 
            {
                if (!remail.IsMatch(tb_e_mail.Text))
                {
                    MessageBox.Show("Invalid e_mail address !", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    tb_e_mail.Text = "";
                }
            }
        }

        private void btn_menu_Click(object sender, EventArgs e)
        {
            this.Hide();
            MainWindow mainWindow = new MainWindow();
            mainWindow.Show();
        }

        private void btn_all_list_Click(object sender, EventArgs e)
        {
            list2listview(LoginedUser.getInstance().User.userPhoneBook);
            listView_phone_book.Show();
            Tb_empty();
        } 
        private void list2listview(List<PhoneBook> pb)
        {
            listView_phone_book.Items.Clear();      
            for (int i = 0; i < pb.Count; i++)
            {
                PhoneBook tmp = new PhoneBook(name, surname, phone_number, address, description, e_mail, LoginedUser.getInstance().User.Username);
                tmp = (PhoneBook)pb[i];
                string[] row = { tmp.Name, tmp.Surname, tmp.Phone_number, tmp.Address, tmp.Description, tmp.E_mail };
                ListViewItem lst_phoneBook = new ListViewItem(row);
                listView_phone_book.Items.Add(lst_phoneBook);
            }
        }
        private void btn_new_contact_Click(object sender, EventArgs e)
        {
            string name = tb_name.Text;
            string surname = tb_surname.Text;
            string phone_number = tb_phone_number.Text;
            string address = tb_address.Text;
            string description = tb_description.Text;
            string e_mail = tb_e_mail.Text;
            bool already_exists = false;
           
            for (int i = 0; i < listView_phone_book.Items.Count; i++)
            {
                if(listView_phone_book.Items[i].SubItems[2].Text.ToString() == tb_phone_number.Text.ToString())
                {
                    already_exists = true;
                    MessageBox.Show(tb_phone_number.Text + " Already Exists ! ");
                }
            }
            if (already_exists==false)
            {
                if (name != "" && surname != "" && phone_number != "" && address != "" && description != "" && e_mail != "")
                {
                    PhoneBook Phone_book = new PhoneBook(name, surname, phone_number, address, description, e_mail,LoginedUser.getInstance().User.Username);
                    LoginedUser.getInstance().User.userPhoneBook.Add(Phone_book);
                    list2listview(LoginedUser.getInstance().User.userPhoneBook);
                    for (int i = 0; i < LoginWindow.userList.Count; i++)
                    {
                        if (LoginWindow.userList[i].Username == LoginedUser.getInstance().User.Username)
                        {
                            LoginWindow.userList[i] = LoginedUser.getInstance().User;
                        }
                    }
                    if (PhoneBookList.Count != 0)
                    {
                        PhoneBookList.Clear();
                    }
                    foreach (User user in LoginWindow.userList)
                    {
                        PhoneBookList.AddRange(user.userPhoneBook);
                    }
                    Util.SaveCsv(PhoneBookList, PhoneBookFilePath);     
                }
                else
                {
                    MessageBox.Show("Error ! ");    //neden 500 tane mesaj kutusu açılıyor ??
                }
             
            }
            Tb_empty();
        }
        private void Tb_empty()
        {
            tb_name.Text = "";
            tb_surname.Text = "";
            tb_phone_number.Text = "";
            tb_address.Text = "";
            tb_description.Text = "";
            tb_e_mail.Text = "";
        }

        private void PhoneBookWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
    }
}
