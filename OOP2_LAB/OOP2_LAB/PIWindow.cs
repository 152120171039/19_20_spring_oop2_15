﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace OOP2_LAB
{
    public partial class PIWindow : Form
    {
        private static string photobase64;
        Stack<CareTakerDatas> undoStack = new Stack<CareTakerDatas>();
        Stack<CareTakerDatas> redoStack = new Stack<CareTakerDatas>();
        public PIWindow()
        {
            InitializeComponent();
        }
        private void PIWindow_Load(object sender, EventArgs e)
        {
            this.KeyPreview = true;
            lbUserInfo.Text = LoginedUser.getInstance().User.Username + "'s Personal Page";
            tbName.Text = LoginedUser.getInstance().User.userPI.Name;
            tbSurname.Text = LoginedUser.getInstance().User.userPI.Surname;
            tbPhone.Text = LoginedUser.getInstance().User.userPI.PhoneNumber;
            tbAddress.Text = LoginedUser.getInstance().User.userPI.Address;
            tbMail.Text = LoginedUser.getInstance().User.userPI.Mail;
            tb_salary.Text = LoginedUser.getInstance().User.userPI.Salary_;
            photobase64 = "";
            if (LoginedUser.getInstance().User.userPI.PhotoBase64 != "No photo")
            {
                pboxUser.Image = Base64ToImage(LoginedUser.getInstance().User.userPI.PhotoBase64);
            }
            btnSave.Enabled = false; 
            Util.SaveCsv(LoginWindow.userList, LoginWindow.userFilePath);
        }
        private Image Base64ToImage(string base64String)
        {
            // Convert base 64 string to byte[]
            byte[] imageBytes = Convert.FromBase64String(base64String);
            // Convert byte[] to Image
            using (var ms = new MemoryStream(imageBytes, 0, imageBytes.Length))
            {
                Image image = Image.FromStream(ms, true);
                return image;
            }
        }
        private string ImageToBase64(Image image, System.Drawing.Imaging.ImageFormat format)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                // Convert Image to byte[]
                image.Save(ms, format);
                byte[] imageBytes = ms.ToArray();

                // Convert byte[] to base 64 string
                string base64String = Convert.ToBase64String(imageBytes);
                return base64String;
            }
        }
        private void PIWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
        private void pboxUser_MouseHover(object sender, EventArgs e)
        {
            if (pboxUser.Image == null)
            {
                toolTipPhoto.Show("Click to select your photo!", pboxUser);
            }
            else
            {
                toolTipPhoto.Show("Click to change your photo!", pboxUser);
            }
        }
        private void pboxUser_Click(object sender, EventArgs e)
        {
            OpenFileDialog opf = new OpenFileDialog();
            opf.Title = "Select Your Photo";
            opf.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png";
            if (opf.ShowDialog() == DialogResult.OK)
            {
                Bitmap image = new Bitmap(opf.FileName);
                pboxUser.Image = image;
                photobase64 = ImageToBase64(pboxUser.Image, pboxUser.Image.RawFormat);
                btnSave.Enabled = true;
            }
        }
        private void tbName_TextChanged(object sender, EventArgs e)
        {
            if (tbName.Text == "")
                btnSave.Enabled = false;
            else
                btnSave.Enabled = true;
        }
        private void tbSurname_TextChanged(object sender, EventArgs e)
        {
            if (tbSurname.Text == "")
                btnSave.Enabled = false;
            else
                btnSave.Enabled = true;
        }
        private void tbPhone_TextChanged(object sender, EventArgs e)
        {
            if (tbPhone.Text.Length < 14)
                btnSave.Enabled = false;
            else
                btnSave.Enabled = true;
        }
        private void tbAddress_TextChanged(object sender, EventArgs e)
        {
            if (tbAddress.Text == "")
                btnSave.Enabled = false;
            else
                btnSave.Enabled = true;
        }
        private void tbMail_TextChanged(object sender, EventArgs e)
        {
            if (IsValidEmail(tbMail.Text))
                btnSave.Enabled = true;
            else
                btnSave.Enabled = false;
        }
        private bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }
        private void tbPassword_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Do you want to change your password?", "Attention", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                tbPassword.Text = "";
                tbPassword.ReadOnly = false;
            }
        }
        private void tbPassword_TextChanged(object sender, EventArgs e)
        {
            if (tbPassword.Text == "")
                btnSave.Enabled = false;
            else
                btnSave.Enabled = true;
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            LoginedUser.getInstance().User.userPI.Name = tbName.Text;
            LoginedUser.getInstance().User.userPI.Surname = tbSurname.Text;
            LoginedUser.getInstance().User.userPI.PhoneNumber = tbPhone.Text;
            LoginedUser.getInstance().User.userPI.Address = tbAddress.Text;
            LoginedUser.getInstance().User.userPI.Mail = tbMail.Text;
            LoginedUser.getInstance().User.userPI.Salary_=tb_salary.Text;
            if (photobase64 != "")
                LoginedUser.getInstance().User.userPI.PhotoBase64 = photobase64;
            if (tbPassword.ReadOnly == false)
            {
                LoginedUser.getInstance().User.Password = Util.ComputeSha256Hash(tbPassword.Text);
            }
            for (int i = 0; i < LoginWindow.userList.Count; i++)
            {
                if (LoginWindow.userList[i].Username == LoginedUser.getInstance().User.Username)
                {
                    LoginWindow.userList[i] = LoginedUser.getInstance().User;
                    break;
                }
            }
            Util.SaveCsv(LoginWindow.userList, LoginWindow.userFilePath);
            MessageBox.Show("Changes saved succesfully!");
            btnSave.Enabled = false;
        }
        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Hide();
            MainWindow mainWindow = new MainWindow();
            mainWindow.Show();
        }
        private void undoRedoOperation(Stack<CareTakerDatas> mainStack, Stack<CareTakerDatas> backUpStack)
        {
            if (mainStack.Count > 0)
            {
                PersonalInformation tempPI = new PersonalInformation();
                CareTakerDatas taker = mainStack.Pop();
                CareTakerDatas oldTaker = new CareTakerDatas();
                tempPI.Undo(taker.Memento);
                tbName.Text = tempPI.Name;
                tbSurname.Text = tempPI.Surname;
                tbPhone.Text = tempPI.PhoneNumber;
                tbAddress.Text = tempPI.Address;
                tbMail.Text = tempPI.Mail;
                if (tempPI.PhotoBase64 != "No photo")
                {
                    pboxUser.Image = Base64ToImage(tempPI.PhotoBase64);
                }
                else
                {
                    pboxUser.Image = null;
                }
                oldTaker.Memento = tempPI.Save();
                backUpStack.Push(oldTaker);

            }
        }

        private void PIWindow_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)26)
            {
                undoRedoOperation(undoStack, redoStack);
            }
            else if (e.KeyChar == (char)25)
            {
                undoRedoOperation(redoStack, undoStack);
            }
            else
            {
                CareTakerDatas data = new CareTakerDatas();
                PersonalInformation tempPI = new PersonalInformation(tbName.Text, tbSurname.Text, tbPhone.Text, tbAddress.Text, tbMail.Text, "No photo", tb_salary.Text);
                if (LoginedUser.getInstance().User.userPI.PhotoBase64 != "No photo")
                {
                    tempPI.PhotoBase64 = ImageToBase64(pboxUser.Image, pboxUser.Image.RawFormat);
                }
                data.Memento = tempPI.Save();
                undoStack.Push(data);
            }
        }
    }
}
